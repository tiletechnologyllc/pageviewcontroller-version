**Page View Controller Version**

As the user swipes forward a new view contorller is created and added to the pages array.

As the user swipes back the last view controller is removed from the pages array.

The short comming here is the lack of reuseablity.

Memory grows steadily until the user begins to swipe back, thus capping the limit on the number of pages a user can opend depending on their device.
