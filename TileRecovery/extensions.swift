import Foundation
import UIKit
import Photos

enum ScrollDirection {
    case right
    case left
}

extension CGRect {
    func dividedIntegral(fraction: CGFloat, from fromEdge: CGRectEdge) -> (first: CGRect, second: CGRect) {
        let dimension: CGFloat
        
        switch fromEdge {
        case .minXEdge, .maxXEdge:
            dimension = self.size.width
        case .minYEdge, .maxYEdge:
            dimension = self.size.height
        }
        
        let distance = (dimension * fraction).rounded(.up)
        var slices = self.divided(atDistance: distance, from: fromEdge)
        
        switch fromEdge {
        case .minXEdge, .maxXEdge:
            slices.remainder.origin.x += 2
            slices.remainder.size.width -= 2
        case .minYEdge, .maxYEdge:
            slices.remainder.origin.y += 2
            slices.remainder.size.height -= 2
        }
        
        return (first: slices.slice, second: slices.remainder)
    }
}

extension UIImage{
    
    func resizeImageWith() -> UIImage {
        let aspectRatio = CGFloat(self.size.width / self.size.height)
        let newWidth = UIScreen.main.bounds.width
        let newSize = CGSize(width: newWidth, height: newWidth/aspectRatio)
        //UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        let renderer = UIGraphicsImageRenderer(size: newSize)
        let image = renderer.image(actions: {(context) in
             self.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        })
       
        //let  newImage = UIGraphicsGetImageFromCurrentImageContext()
        //UIGraphicsEndImageContext()
        return image
    }
    
//     func compressFileSize(sizeInKB:Int){
//        var compressionQuality:CGFloat = 1.0
//        if var data = self.jpegData(compressionQuality: compressionQuality) as Data? {
//            var count = data.count
//            while count > sizeInKB*1000{
//            print("There were \(data.count) bytes")
//            let bcf = ByteCountFormatter()
//            bcf.allowedUnits = [.useKB] // optional: restricts the units to MB only
//            bcf.countStyle = .file
//            let string = bcf.string(fromByteCount: Int64(data.count))
//            print("formatted result: \(string)")
//            compressionQuality *= 0.75
//            data = self.jpegData(compressionQuality: compressionQuality)!
//            count = data.count
//            }
//        }
//    }
    
    func compressDataFileSize() -> Data {
        var actualHeight = self.size.height
        var actualWidth = self.size.width
        let maxHeight: CGFloat = UIScreen.main.bounds.height
        let maxWidth: CGFloat = UIScreen.main.bounds.width
        var imgRatio: CGFloat = actualWidth / actualHeight
        let maxRatio: CGFloat = maxWidth / maxHeight
        let compressionQuality: CGFloat = 0.25
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        let size = CGSize(width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        let renderer = UIGraphicsImageRenderer(size: size)
        let imageData = renderer.jpegData(withCompressionQuality: compressionQuality, actions: {(context) in
            self.draw(in: rect)
        })
        print("rect: \(rect)")
        //        let image = renderer.image(actions: {(context) in
        //            self.draw(in: rect)
        //        })
        let image = UIImage(data: imageData) ?? UIImage()
        // print("Bytes: \(imageData.count)")
        print("imageBytesCount: \(image.jpegData(compressionQuality: 1.0)?.count)")
        //return image
        return imageData
    }
}


extension UIImageView{
    func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize) {
        let options = PHImageRequestOptions()
        options.resizeMode = .exact
        options.version = .original
        //options.deliveryMode = .highQualityFormat
        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { image, _ in
            guard let image = image else { return }
            switch contentMode {
            case .aspectFill:
                self.contentMode = .scaleAspectFill
            case .aspectFit:
                self.contentMode = .scaleAspectFit
            }
            self.image = image
        }
    }
}


extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
