//
//  ReloadSegementedViewController-Delegate.swift
//  TileRecovery
//
//  Created by 123456 on 2/13/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol ReloadSegmentedViewControllerDelegate: class {
    func reloadSegmentedViewController()
}

