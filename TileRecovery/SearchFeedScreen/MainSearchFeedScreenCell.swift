//
//  MainSearchFeedScreenCell.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//


import Foundation
import UIKit

class MainSearchFeedScreenCell:UICollectionViewCell{
    var containerView:UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        containerView = UIView(frame: self.frame)
        self.addSubview(containerView)
        containerView.backgroundColor = backgroundColor
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
