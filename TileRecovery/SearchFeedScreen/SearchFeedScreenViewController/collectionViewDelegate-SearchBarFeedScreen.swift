//
//  collectionViewDelegate-SearchBarFeedScreen.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

extension SearchFeedScreenViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("item: \(indexPath.item)\n\ttag: \(tagsArray[indexPath.item])")
        print("item: \(indexPath.item)\n\ttag: \(tagsUsersArray[indexPath.item])")
//        self.selectedTag = tagsArray[indexPath.item]
//        self.selectedTag = tagsUsersArray[indexPath.item]
        if let selectedTag = tagsUsersArray[indexPath.item] as? Tag{
            self.selectedTag = selectedTag
            // self.addOrRemoveCellDelegate?.addCell(sender: self, cellType: CellTypes.MainSearchFeedScreenCell)
            self.addOrRemoveCellDelegate?.addCell(sender: self, tag: selectedTag)
        }else if let selectedUser = tagsUsersArray[indexPath.item] as? User{
            self.selectedUser = selectedUser
            self.addOrRemoveCellDelegate?.addCell(sender: self, user: selectedUser)
           // self.addOrRemoveCellDelegate?.addCell(sender: selectedUser, cellType: CellTypes.MainSearchFeedScreenCell)
        }else{
            print("Selected tag was a user adjust accordingly dela iwth it")
        }
        collectionView.isUserInteractionEnabled = false
    }
}
