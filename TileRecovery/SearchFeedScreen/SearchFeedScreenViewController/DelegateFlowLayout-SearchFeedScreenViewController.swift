//
//  DelegateFlowLayout-SearchFeedScreenViewController.swift
//  Spreee
//
//  Created by Thomas M. Jumper on 9/18/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

extension SearchFeedScreenViewController:UICollectionViewDelegateFlowLayout{
    func setUpLayout(){
        feedSearchBar.translatesAutoresizingMaskIntoConstraints = false
        feedSearchBar.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50).isActive = true
        feedSearchBar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16).isActive = true
        feedSearchBar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16).isActive = true
        feedSearchBar.heightAnchor.constraint(equalToConstant: 45).isActive = true
        //feedSearchBar.heightAnchor.constraint(equalToConstant: 0).isActive = true
        
        
        magnigyingGlassImageView.translatesAutoresizingMaskIntoConstraints = false
        magnigyingGlassImageView.leadingAnchor.constraint(equalTo: feedSearchBar.leadingAnchor).isActive = true
        magnigyingGlassImageView.heightAnchor.constraint(equalTo: feedSearchBar.heightAnchor).isActive = true
        magnigyingGlassImageView.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        placeholder.translatesAutoresizingMaskIntoConstraints = false
        placeholder.leadingAnchor.constraint(equalTo: magnigyingGlassImageView.trailingAnchor).isActive = true
        placeholder.centerYAnchor.constraint(equalTo: feedSearchBar.centerYAnchor).isActive = true
        placeholder.heightAnchor.constraint(equalTo: feedSearchBar.heightAnchor).isActive = true
        
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        //collectionView.topAnchor.constraint(equalTo: self.feedSearchBar.topAnchor, constant: 0).isActive = true
        collectionView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // Number of cells
        let collectionViewWidth = collectionView.bounds.width/4.0
        let collectionViewHeight = collectionViewWidth
        
        print("cell height should be set to: \(collectionView.bounds.width/4.0+25)")
        print("cell width should be set to: \(collectionView.bounds.width/4.0-4)")
        
        return CGSize(width: collectionView.bounds.width/3.0-3-8, height: collectionView.bounds.width/3.0-3-8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.contentSize.width, height: 2.4*self.feedSearchBar.frame.height)
        //   return CGSize(width: collectionView.contentSize.width, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8)
    }
    
    
}
