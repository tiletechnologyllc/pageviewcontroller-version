//
//  TextFieldDelegate-SearchFeedScreenViewController.swift
//  Spreee
//
//  Created by Thomas M. Jumper on 9/18/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import FirebaseDatabase
import UIKit

extension SearchFeedScreenViewController:UITextFieldDelegate{
    //New stuff
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        //        currentGenericArray = genericArray.filter({letter -> Bool in
        //            letter.lowercased().contains(searchBar.text!)
        //        })
        //        if currentGenericArray.isEmpty{
        //            print("This element is not in the current array")
        //        }
        collectionView.reloadData()
        return true
    }
    
    /// Helper to dismiss keyboard
    @objc func didStopEditing() {
        // textFieldShouldReturn(phoneNumberTextField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.setAnimationCurve(UIView.AnimationCurve.easeInOut)
        UIView.animate(withDuration: 0.2) {
            self.view.frame.origin.y = 0
        }
        //   self.feedSearchBar.attributedPlaceholder = NSAttributedString(string: "Tap To Search",
        //                                                              attributes: [NSAttributedStringKey.foregroundColor: colors.toolBarColor])
        self.placeholder.isHidden = false
    }
    
    @objc func textFieldDidChange(){
        print("inside of text field did change")
        
        guard let feedSearchBarText = feedSearchBar.text else{
            print("feedSearchBar.text is nil")
            return
        }
        
        guard(!feedSearchBarText .isEmpty) else{
            print("The search bar is empty")
           // currentTagsArray = genericTagsArray
//            tagsArray = initialTagsArray
            self.tagsUsersArray = initialTagsUsersArray
            collectionView.reloadData()
            return
        }
        print("The search bar was not empty it says: \(String(describing: feedSearchBar.text))")
        
//         tagsArray = initialTagsArray.filter({tag -> Bool in
//            //letter.lowercased().contains(searchBar.text!)
//            //mew stuff
//             if feedSearchBar.text!.count > tag.name.count{
//                return false
//            }
//            let stringRange = tag.name.index(tag.name.startIndex, offsetBy: feedSearchBar.text!.count)
//            let subword = tag.name[..<stringRange]
//            return subword.lowercased().contains(feedSearchBar.text!.lowercased())
//            //
//            //If broken delete above and uncoment below
//            //letter.lowercased().contains(searchBar.text!.lowercased())
//
//        })
        
        tagsUsersArray = initialTagsUsersArray.filter({tag -> Bool in
                if let tag = tag as? Tag{
                    if feedSearchBar.text!.count > tag.name.count{
                        return false
                    }
                    let stringRange = tag.name.index(tag.name.startIndex, offsetBy: feedSearchBar.text!.count)
                    let subword = tag.name[..<stringRange]
                    return subword.lowercased().contains(feedSearchBar.text!.lowercased())
                }
                else if let user = tag as? User{
                    if feedSearchBar.text!.count > user.username.count{
                        return false
                    }
                    let stringRange = user.username.index(user.username.startIndex, offsetBy: feedSearchBar.text!.count)
                    let subword = user.username[..<stringRange]
                    return subword.lowercased().contains(feedSearchBar.text!.lowercased())
                }
            return false
            })
        
        if currentTagsArray.isEmpty{
            //currentTagsArray.insert(feedSearchBar.text!, at: 0)
            print("The tags array is empty figure out what to do: \(initialTagsUsersArray)")
            searchDataBase(name: feedSearchBarText)
            self.collectionView.reloadData()
        }
        
        collectionView.reloadData()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //    textField.placeholder = ""
        placeholder.isHidden = true
    }

    
    func searchDataBase(name:String){
        print("Search Database called")
        let searchName = name.lowercased()
        
        let dispatchGroup:DispatchGroup = DispatchGroup()
        
        let ref = Database.database().reference(withPath: "TagIDs")
        print("name: \(name), name + ~: \(name + "~")")
        let startValue = searchName
        let endValue = searchName + "~"
        
        var isItemFound = false
        
        
        dispatchGroup.enter()
        Database.database().reference(withPath: "TagIDs").queryOrdered(byChild: "name").queryStarting(atValue: startValue).queryEnding(atValue: endValue).observeSingleEvent(of: .value, with: {(snapshot) in
            
            if !snapshot.exists() || snapshot.childrenCount == 0{
                print("The snap shot is empty make a new tag")
//                self.tagsUsersArray.insert(Tag(name: name, location:["longitude" :self.userLocation.longitude, "latitude":self.userLocation.latitude] , tagID: name, storageRef: "")! , at: 0)
//                self.collectionView.reloadData()
                dispatchGroup.leave()
                return
            }
            
            isItemFound = true
            
            print("snapshot values: \(snapshot.value)")
            let data = snapshot.value as? [[String:Any]]
            let l = snapshot.value as? [String:Any]
            let d = snapshot.value as? [Any]
            print("Children count \(snapshot.childrenCount)")
            print("Data L: \(l)")
            print("Data d: \(d)")
            print("Data returned from query: \(data)")
            
            for child in snapshot.children{
                let node = child as? DataSnapshot
                print("Key: \(node?.key)")
                guard let tagID = node?.key else{
                    print("no tagID")
                    continue
                }
                print("Child in snapshot: \(node?.value)")
                
                guard let nodeData = node?.value as? [String:Any] else{
                    print("unable to convert nodeData to [String:Any]: \(node?.value)")
                    continue
                }
                
                guard let name = nodeData["name"] as? String, let location = nodeData["location"] as? [String:Any], let storageRef = nodeData["storageRef"] as? String, let photoURL = nodeData["photoURL"] as? String else{
                    return print("Unable to parse firebase data2")
                }
                
                guard let tag = Tag(name: name, location: location, tagID: tagID, storageRef: storageRef, photoURL: photoURL) else{
                    print("Error parsing tag")
                    continue
                }
                let hasTag = self.tagsUsersArray.contains(where: {(tg) in
                    if let tg = tg as? Tag{
                        return tg.tagID == tag.tagID
                    }
                    else{
                        return false
                    }
                })
                
                if hasTag{
                    return
                }
                print("TAg about to be added to array: \(tag)")
//                self.tagsArray.append(tag)
                self.tagsUsersArray.append(tag)
//                self.collectionView.reloadData()
            }
            dispatchGroup.leave()
        }, withCancel: {(error) in
            print("Error getting the snapshot \(error.localizedDescription)")
//            self.tagsUsersArray.insert(Tag(name: name, location:["longitude" :self.userLocation.longitude, "latitude":self.userLocation.latitude] , tagID: name, storageRef: "")! , at: 0)
            dispatchGroup.leave()
        })
        
        var tempArr:[Any] = [Any]()
        dispatchGroup.enter()
        Database.database().reference(withPath: "Users").queryOrdered(byChild: "username").queryStarting(atValue: startValue).queryEnding(atValue: endValue).observeSingleEvent(of: .value, with: {(snapshot) in
            if !snapshot.exists() || snapshot.childrenCount == 0{
                print("The snap shot is empty make a new tag")
            //    self.tagsUsersArray.insert(User(userID: name, username: name, photoURL: User.defaultProfilePhotoURL), at: 0)
              //  self.collectionView.reloadData()
                dispatchGroup.leave()
                return
            }
            
            isItemFound = true
            
            for child in snapshot.children{
                let node = child as? DataSnapshot
                print("Key: \(node?.key)")
                guard let userID = node?.key else{
                    print("no userID")
                    continue
                }
                
                print("Child in snapshot: \(node?.value)")
                
                guard let nodeData = node?.value as? [String:Any] else{
                    print("unable to convert nodeData to [String:Any]: \(node?.value)")
                    continue
                }
                
                guard let username = nodeData["username"] as? String, let photoURL = nodeData["photoURL"] as? String else{
                    return print("Unable to parse firebase data2 username")
                }
                
               let user = User(userID: userID, username: username, photoURL: photoURL)
                
                let hasUser = self.tagsUsersArray.contains(where: {(tg) in
                    if let tg = tg as? User{
                        return tg.userID == user.userID
                    }else{
                        return false
                    }
                })
                
                if hasUser{
                    return
                }
                print("TAg about to be added to array: \(user)")
                tempArr.append(user)
                //self.tagsUsersArray.append(user)
            }
            dispatchGroup.leave()
        }, withCancel: {(error) in
            print("Error searching database for username: \(error)")
          //  self.tagsUsersArray.insert(User(userID: name, username: name, photoURL: User.defaultProfilePhotoURL), at: 0)
            dispatchGroup.leave()
        })
        
        dispatchGroup.notify(queue: .main, execute: {
            if isItemFound == false{
                self.tagsUsersArray.insert(Tag(name: name, location:["longitude" :self.userLocation.longitude, "latitude":self.userLocation.latitude] , tagID: name, storageRef: "", photoURL:"")! , at: 0)
            }
            for user in tempArr{
                if let user = user as? User{
                    self.isUserBlocked(userID: user.userID, completion: {(isBlocked) in
                        if isBlocked{
                            return
                        }else{
                          let isUserAlreadyInArray =  self.tagsUsersArray.contains(where: { (element) -> Bool in
                                if let element = element as? User{
                                    return element.userID == user.userID
                                }else{
                                    return false
                                }
                            })
                            if isUserAlreadyInArray == false{
                                self.tagsUsersArray.append(user)
                            }
                        }
                    })
                }else{
                        self.tagsUsersArray.append(user)
                }
            }
            tempArr.removeAll()
            self.collectionView.reloadData()
            })
    }
    
    func isUserBlocked(userID:String, completion: @escaping ((Bool) -> Void)){
        Database.database().reference(withPath: "BlockedUsers/\(User.signedInUserID)/\(userID)").observe(.value, with: {(dataSnapshot) in
            if dataSnapshot.exists(){
                completion(true)
            }else{
                completion(false)
            }
        }, withCancel: {(error) in
            print("An error occured getting the blockedUsers: \(error)")
                completion(false)
        })
    
    }
}

