import Foundation
import UIKit
import FirebaseUI

extension SearchFeedScreenViewController:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! Header
        //header.backgroundColor = .blue
        //header.backgroundColor =  colors.toolBarColor
        return header
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return self.tagsUsersArray.count
        //return tagsArray.count
        // return currentTagsArray.count
        // return imagePost.defaultImages.count
    }
    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "feedViewCell", for: indexPath) as! feedViewCell
//
//
//        cell.feedCellImageView.backgroundColor = .blue
//        cell.feedCellImageView.clipsToBounds = true
//        //cell.feedCellImageView.layer.cornerRadius = CGFloat((cell.feedCellImageView.frame.width)/5)
//        cell.feedCellImageView.image = imagePost.defaultImages[indexPath.item].image
//        print("feed cell actual width: \(cell.frame.width)")
//        print("feed cell actual height: \(cell.frame.height)")
//        print("feed cell corner radius: \(cell.feedCellImageView.layer.cornerRadius )")
//        cell.feedCellImageView.layer.cornerRadius  = 29.6
//
//        cell.feedCellImageView.contentMode = .scaleAspectFill
//
//        guard let image = cell.feedCellImageView.image else{
//            print("Error in cellforitemat in SearchfeedScreena cella")
//            return UICollectionViewCell()
//        }
//
//        let photoAspectRatio:CGFloat = image.size.width/image.size.height
//        let cellAspectRatio:CGFloat = cell.bounds.width/cell.bounds.height
//        if photoAspectRatio >= cellAspectRatio{
//            cell.feedCellImageView.frame = CGRect(x: 0, y: 0, width: photoAspectRatio*cell.bounds.height, height: cell.bounds.height)
//            cell.feedCellImageView.centerXAnchor.constraint(equalTo: cell.centerXAnchor, constant: 0).isActive = true
//
//        }
//        else{
//            cell.feedCellImageView.frame = CGRect(x: 0, y: 0, width: cell.bounds.width, height: cell.bounds.width/photoAspectRatio)
//            cell.feedCellImageView.centerYAnchor.constraint(equalTo: cell.centerYAnchor, constant: 0).isActive = true
//
//        }
//
//
//        //cell.feedCellLabel.text = "Tags Go Here this is a really long tag that will exceed the bounds"
//
//        cell.feedCellLabel.text = currentTagsArray[indexPath.item]
//        return cell
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "feedViewCell", for: indexPath) as! feedViewCell
        
//        print("Tags about to be displayed: \(tagsArray)")
        print("Tags Users about to be displayed: \(self.tagsUsersArray)")
        cell.backgroundColor = colors.backgroundColor
        
        //let image = imagePost.defaultImages[indexPath.item].image!
       // let image = imagePost.defaultImages[indexPath.item].image!
        
        cell.feedCellImageView.contentMode = .scaleAspectFill
//        cell.feedCellLabel.text = genericTagsArray[indexPath.item]
        
        let tagUser = self.tagsUsersArray[indexPath.item]
        
        var storagePath:String!
        var name:String!
        var url:String!
        
        if let tagUser = tagUser as? Tag{
            storagePath = tagUser.storageRef
            name = tagUser.name
            url = tagUser.photoURL
        }else if let tagUser = tagUser as? User{
            storagePath = "Profile Pictures/" + tagUser.storagePath
            name = tagUser.username
            url = tagUser.photoURL
        }else{
            print("Unable to return either tag or user")
            assertionFailure("Unable to return either tag or user")
        }
        
        //let storagePath = tagsArray[indexPath.item].storageRef
        let storageRef = self.storage.reference(withPath: storagePath)
        
        let placeholder:UIImage = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
        
//        cell.feedCellImageView.sd_setImage(with: storageRef, placeholderImage: placeholder, completion: {(image,error,imageCacheType,storageReference) in
//            if let error = error{
//                print("Uh-Oh an error has occured: \(error.localizedDescription)" )
//            }
//        })
        
        cell.feedCellImageView.sd_setImage(with: URL(string: url), placeholderImage: placeholder, options: SDWebImageOptions.refreshCached, completed: {(image,error,imageCacheType,storageReference) in
                        if let error = error{
                            print("Uh-Oh an error has occured: \(error.localizedDescription)" )
                        }
              //  self.setImageToAspectRatio(image: image, imageView: cell.feedCellImageView, bounds: cell.bounds)
        })
        
        //print("name in cell: \(tagsArray[indexPath.item].name)")
        print("name in cell: \(name)")
        //cell.feedCellLabel.text = tagsArray[indexPath.item].name
        cell.feedCellLabel.text = name

        print("\tCell Image View Frame: \(cell.feedCellImageView.frame)")
        
        cell.feedCellImageView.clipsToBounds = true
        cell.clipsToBounds = true
        cell.feedCellImageView.layer.cornerRadius  = 29.6
        cell.feedCellImageView.layer.masksToBounds = true
        cell.layer.cornerRadius = 29.6
        return cell
    }
    
    func setImageToAspectRatio(image:UIImage?,imageView:UIImageView,bounds:CGRect){
        guard let image = image else{
            print("Image was nil")
            let placeHolder = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
            imageView.image = placeHolder
            return
        }
        let photoAspectRatio = image.size.width/image.size.height
        let cellAspectRatio = bounds.width/bounds.height
        if photoAspectRatio >= cellAspectRatio{
            imageView.frame = CGRect(x: 0, y: 0, width: photoAspectRatio*bounds.height, height: bounds.height)
        }
        else{
            imageView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.width/photoAspectRatio)
        }
    }
    
}


