//
//  SearchFeedScreen-LocationDelegate.swift
//  TileRecovery
//
//  Created by 123456 on 12/12/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseFunctions



protocol SearchFeedScreenLocationDelegate: class {
    func setLocation(location:GeoPoint)
}

extension SearchFeedScreenViewController:SearchFeedScreenLocationDelegate{
    
    func setLocation(location: GeoPoint) {
        self.userLocation = location
        print("Location set inside of thje searhc feedscreenViewController: \(location)")
        self.getGeoHashQueries(centerPoint: location)
    }
    
    func getGeoHashQueries(centerPoint:GeoPoint){
        print("inside of get feoHashQueries")
        self.functions.httpsCallable("geohashQueries").call(["latitude": centerPoint.latitude,"longitude":centerPoint.longitude], completion: {(result, error) in
            print("Inside of dunctions function")
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                    print("An error occured getting the data:: \(String(describing: code)) , \(message), \(String(describing: details))")
                }
                // ...
                return
            }
            if result?.data == nil{
                print("No data returned from geohash queries")
                return
            }
            let data = result?.data as? [[String:Any]]

            print("Data: \(data) \n\n")
//                        self.tagsArray.removeAll()
//                        self.initialTagsArray.removeAll()
            self.tagsUsersArray.removeAll()
            self.initialTagsUsersArray.removeAll()
            
            for element in data!{

                print("Storage ref \(element["storageRef"])")

                guard let name = element["name"] as? String,
                    let tagID = element["tagID"] as? String,
                    let storageRef = element["storageRef"] as? String,
                    let location = element["location"] as? [String:Any],
                    let photoURL = element["photoURL"] as? String else{
                        print("Unable to parse firebase data1")
                        continue
                        //return print("Unable to parse firebase data")
                }
                guard let tag = Tag(name: name, location: location, tagID: tagID,storageRef: storageRef,photoURL:photoURL) else{
                    print("Error parsing tag")
                    return
                }
                //  self.currentTagsArray.append(tag)
                self.tagsUsersArray.append(tag)
//                self.tagsArray.append(tag)
                self.initialTagsUsersArray.append(tag)
//                self.initialTagsArray.append(tag)
            }
            print("Restaurants Near By: \(self.tagsUsersArray.description)")
            print("Restuarant names: ")
            for restaurant in self.tagsUsersArray{
                if let tag = restaurant as? Tag{
                    print(tag.name)
                }else if let user = restaurant as? User{
                    print(user.username)
                }else{
                    print("neither user nor tag")
                }
            }
            self.collectionView.reloadData()
        })
        
//        db.reference(withPath: "TagIDs").queryOrdered(byChild: "totalPosts").queryStarting(atValue: 1).observeSingleEvent(of: .value, andPreviousSiblingKeyWith: {(snapshot,string) in
//            print("Snapshot: \(snapshot)")
//            let data = snapshot.value as? [String:Any]
//            for (key,value) in data!{
//                print("key: \(key), => value: \(value)")
//
//                let tagID = key
//                let element = value as! [String:Any]
//                print("Element: \(element)")
//                          guard let name = element["name"] as? String,
//                                let storageRef = element["storageRef"] as? String,
//                                let location = element["location"] as? [String:Any] else{
//                                    print("Unable to parse firebase data1")
//                                    continue
//                                    //return print("Unable to parse firebase data")
//                            }
//                                guard let tag = Tag(name: name, location: location, tagID: tagID,storageRef: storageRef) else{
//                                    print("Error parsing tag")
//                                    return
//                                }
//                                //  self.currentTagsArray.append(tag)
//                                self.tagsArray.append(tag)
//                                self.initialTagsArray.append(tag)
//                            }
//                            print("Restaurants Near By: \(self.currentTagsArray.description)")
//                            self.collectionView.reloadData()
//
//        }, withCancel: {(error) in
//            print("an error occured while performing query: \(error)")
//        })
//
   }
}
