//
//  UIScrollViewDelegate-SearchFeedScreenViewController.swift
//  TileRecovery
//
//  Created by 123456 on 2/14/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension SearchFeedScreenViewController:UIScrollViewDelegate{
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            self.loadMore()
        }
    }
    
    func loadMore(){
        print("add in load more functionality")
        //make this global and record in last getFollowing call
        var endValue:String = ""
    }
}
