//
//  FeedViewCell.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

class feedViewCell:UICollectionViewCell{
    var feedCellImageView: UIImageView!
    var feedCellLabel: UILabel!
    var topImageViewAnchor:NSLayoutConstraint!
    var leadingImageViewAnchor:NSLayoutConstraint!
    var trailingImageViewAnchor:NSLayoutConstraint!
    var  bottomImageViewAnchor: NSLayoutConstraint!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //        self.layer.borderColor = UIColor.red.cgColor
        //        self.layer.borderWidth = 3
        
        feedCellImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        feedCellLabel = UILabel(frame: CGRect(x: 0, y: 119, width: 134, height: 20.5))
        
        feedCellLabel.textAlignment = .center
        feedCellLabel.textColor = colors.backgroundColor
        self.addSubview(feedCellImageView)
        self.feedCellImageView.addSubview(feedCellLabel)
       
        self.feedCellImageView.translatesAutoresizingMaskIntoConstraints = false
        feedCellImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        feedCellImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        feedCellImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        feedCellImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
//         feedCellImageView.translatesAutoresizingMaskIntoConstraints = false
//        topImageViewAnchor = feedCellImageView.topAnchor.constraint(equalTo: self.topAnchor)
//        topImageViewAnchor.isActive = true
//        
//        leadingImageViewAnchor = feedCellImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor)
//        leadingImageViewAnchor.isActive = true
//        
//        trailingImageViewAnchor = feedCellImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor)
//        trailingImageViewAnchor.isActive = true
//        //feedCellImageView.bottomAnchor.constraint(equalTo: self.feedCellLabel.topAnchor).isActive = true
//        bottomImageViewAnchor = feedCellImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
//        bottomImageViewAnchor.isActive = true

       
        feedCellLabel.translatesAutoresizingMaskIntoConstraints = false
        feedCellLabel.leadingAnchor.constraint(equalTo: self.feedCellImageView.leadingAnchor).isActive = true
        feedCellLabel.trailingAnchor.constraint(equalTo: self.feedCellImageView.trailingAnchor).isActive = true
        feedCellLabel.bottomAnchor.constraint(equalTo: self.feedCellImageView.bottomAnchor).isActive = true
        feedCellLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        // feedCellLabel.topAnchor.constraint(equalTo: self.feedCellImageView.bottomAnchor).isActive = true
        
        //        feedCellLabel.layer.borderColor = UIColor.green.cgColor
        //        feedCellLabel.layer.borderWidth = 3
        
        self.bringSubviewToFront(feedCellLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

