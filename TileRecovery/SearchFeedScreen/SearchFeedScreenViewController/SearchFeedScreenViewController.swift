//
//  ViewController.swift
//  FeedScreen
//
//  Created by 123456 on 6/24/18.
//  Copyright © 2018 123456. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class SearchFeedScreenViewController: UIViewController{

    var userLocation:GeoPoint!
    
    var genericTagsArray:[Tag]!
//    var genericTagsArray:[String] = ["thomas","MariaC","MariaJ","Zachary","tag5","tag6","tag7","tag8","tag9","tag10","tag11","tag12","A","B","C","D","E","F","G","Ab","Abc","za","tag1","tag2","tag3","tag4","tag5","tag6"]//,"tag7","tag8","tag9","tag10","tag11","tag12","A","B","C","D","E","F","G","Ab","Abc","za"]
    var currentTagsArray:[String] = [String]()
    var tagsSelected:[String] = [String]()
    
//    var initialTagsArray:[Tag] = [Tag]()
//    var tagsArray:[Tag] = [Tag]()
    
    var initialTagsUsersArray:[Any] = [Any]()
    var tagsUsersArray:[Any] = [Any]()
    
    var db:Database!
    
    weak var addOrRemoveCellDelegate:addOrRemoveCell?
    
    let keyboardSlider = KeyboardSlider()
    var collectionView:UICollectionView!
    var offsetY:CGFloat = 0
    var feedSearchBar: UITextField!
    
    var placeholder : UILabel!
    let magnigyingGlassImageView = UIImageView()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    lazy var functions = Functions.functions()
    var storage:Storage!
    
    var selectedTag:Tag?{
        didSet{
            print("Selected tagID in searchFeedScreem: \(selectedTag?.tagID)")
        }
    }
    
    var selectedUser:User?{
        didSet{
            print("Selected userID in searchFeedScreen: \(selectedUser?.userID)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.db = Database.database()
        storage = Storage.storage()
        // Do any additional setup after loading the view, typically from a nib.
        keyboardSlider.subscribeToKeyboardNotifications(view: view)
        
        //collectionView
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 2.5, bottom: 0, right: 2.5)
        layout.minimumLineSpacing = 0
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 375, height: 637), collectionViewLayout: layout)
        self.view.addSubview(collectionView)
        collectionView.register(feedViewCell.self, forCellWithReuseIdentifier: "feedViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(gestureRecognizer:)))
        viewTapGestureRecognizer.cancelsTouchesInView = false
        self.collectionView.addGestureRecognizer(viewTapGestureRecognizer)
        collectionView.backgroundColor = colors.backgroundColor
        //Styling the Search Bar
        let searchBarHeight = 45
        self.feedSearchBar = UITextField(frame: CGRect(x: 20, y: 0, width: Int(self.collectionView.bounds.width), height: searchBarHeight*2))
        //self.feedSearchBar.frame = CGRect(x: 20, y: feedSearchBar.frame.maxY, width: self.collectionView.bounds.width, height: searchBarHeight*2)
        self.view.addSubview(feedSearchBar)
        self.feedSearchBar.borderStyle = .roundedRect
        self.feedSearchBar.layer.cornerRadius = feedSearchBar.frame.width/20
        feedSearchBar.borderStyle = .none
        //        self.feedSearchBar.layer.borderColor = UIColor.white.cgColor
        //        self.feedSearchBar.layer.borderWidth = 4
        //    self.feedSearchBar.placeholder = "Tap To Search"
        //       self.feedSearchBar.attributedPlaceholder = NSAttributedString(string: "Tap To Search",
        //                                                                     attributes: [NSAttributedStringKey.foregroundColor: colors.toolBarColor])
        
        //feedSearchBar = UITextField(frame: CGRect( x: 20, y: 100, width: self.view.bounds.width, height: 30))
        feedSearchBar.backgroundColor = .green
        placeholder = UILabel(frame: CGRect( x: 20, y: 0, width: feedSearchBar.bounds.width, height: feedSearchBar.bounds.height))
        placeholder.text = "Search"
        placeholder.font = UIFont.boldSystemFont(ofSize: 25)
        placeholder.textColor = colors.toolBarColor
        placeholder.isHidden = false
        placeholder.textAlignment = .left
        feedSearchBar.addSubview(placeholder)
        print("feedSearchBarText: \(feedSearchBar.text)")
        if feedSearchBar.text == nil || !(feedSearchBar.text ?? "").isEmpty{
            print("feedSearchBarText: \(feedSearchBar.text)")
            feedSearchBar.isHidden = true
        }else{
            print("feedSearchBarText was nil")
        }
        feedSearchBar.delegate = self
        
        
        
        
        self.feedSearchBar.returnKeyType = .search
        self.feedSearchBar.delegate = self
       // currentTagsArray = genericTagsArray
        
        feedSearchBar.textColor = colors.toolBarColor
        
        feedSearchBar.autocorrectionType = .no
        feedSearchBar.keyboardType = .default
        feedSearchBar.addTarget(self, action: #selector(SearchFeedScreenViewController.textFieldDidChange), for: .editingChanged)
        
        feedSearchBar.backgroundColor = UIColor.clear
        feedSearchBar.tintColor = colors.toolBarColor
        feedSearchBar.font = UIFont.boldSystemFont(ofSize: 20)
        //Code to add magnifying glass on right side
        //looking lgass
        
        let magnifyingGlassImage = #imageLiteral(resourceName: "icons8-widesearch-filled-100")
        magnigyingGlassImageView.image = magnifyingGlassImage
        //arrange the frame according to your textfield height and image aspect
        magnigyingGlassImageView.frame = CGRect(x: 0, y: 5, width: 45, height: 20)
        feedSearchBar.addSubview(magnigyingGlassImageView)
        magnigyingGlassImageView.contentMode = .scaleAspectFit
        feedSearchBar.leftViewMode = .always
        feedSearchBar.leftView = magnigyingGlassImageView
        self.feedSearchBar.rightViewMode = .always
        
        //Adding in a header
        collectionView.register(Header.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        self.view.bringSubviewToFront(feedSearchBar)
        
        
        
        self.collectionView.contentInsetAdjustmentBehavior = .never
        
        setUpLayout()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    var lastContentOffset:CGFloat = 0
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.lastContentOffset > self.collectionView.contentOffset.y  && self.collectionView.contentOffset.y > 0 && self.collectionView.contentOffset.y < collectionView.frame.maxY {
            
            self.lastContentOffset = scrollView.contentOffset.y
            feedSearchBar.transform = CGAffineTransform.identity
        }
        else if (self.lastContentOffset < self.collectionView.contentOffset.y) && (self.collectionView.contentOffset.y < self.collectionView.frame.maxY) && (self.collectionView.contentOffset.y > 0)  {
            self.lastContentOffset = scrollView.contentOffset.y
            feedSearchBar.transform = CGAffineTransform(translationX: 0, y: -self.collectionView.contentOffset.y)
        }
        else{
            self.lastContentOffset = scrollView.contentOffset.y
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(SearchFeedScreenViewController.keyboardFrameChangeNotification(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    
    
    
    @objc func keyboardFrameChangeNotification(notification: Notification) {
        
    }
    
    
    @objc func viewTapped(gestureRecognizer:UIGestureRecognizer){
        
        if  feedSearchBar.isFirstResponder{
            feedSearchBar.resignFirstResponder()
        }
    }
    
}




