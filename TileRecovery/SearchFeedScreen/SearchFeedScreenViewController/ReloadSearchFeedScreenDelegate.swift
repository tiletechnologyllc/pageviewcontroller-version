//
//  ReloadSearchFeedScreenDelegate.swift
//  TileRecovery
//
//  Created by 123456 on 2/21/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol ReloadSearchFeedScreenDelegate: class {
    func reloadSearchFeedScreen()
}

extension MainViewController:ReloadSearchFeedScreenDelegate{
    func reloadSearchFeedScreen() {
//        self.searchFeedScreenViewController.initialTagsUsersArray.removeAll()
//        self.searchFeedScreenViewController.tagsUsersArray.removeAll()
        self.searchFeedScreenViewController.getGeoHashQueries(centerPoint: self.userLocation)
    }
}
