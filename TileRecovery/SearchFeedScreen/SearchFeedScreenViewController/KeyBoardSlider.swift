//
//  KeyBoardSlider.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

//extension Notification.Name{
//    static let showKeyboard = Notification.Name("showKeyboard")
//}
class KeyboardSlider: NSObject {
    // variables to hold and process information from the view using this class
    weak var view: UIView?
    
    @objc func keyboardWillShow(notification: NSNotification) {
        // method to move keyboard up
        // view?.frame.origin.y = 0 - getKeyboardHeight(notification as Notification)
        print("made it to keyboard will show")
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        // get exact height of keyboard on all devices and convert to float value to return for use
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        return keyboardSize.cgRectValue.height
    }
    
    func subscribeToKeyboardNotifications(view: UIView) {
        // assigning view to class' counterpart
        self.view = view
        // when UIKeyboardWillShow do keyboardWillShow function
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
}

