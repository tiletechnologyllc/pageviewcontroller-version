//
//  FeedTableViewCell.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import UIKit

class FeedTableViewCell: UITableViewCell {
    
    //    @IBOutlet weak var cellImageView: UIImageView!
    //    @IBOutlet weak var cellLabel: UILabel!
    
    @IBOutlet weak var cellImageView: UIImageView!
    
    @IBOutlet weak var cellLabel: UILabel!
    override func awakeFromNib() {
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        // cellLabel.backgroundColor = colors.backgroundColor
        
    }
    
}
