//
//  FeedTableViewController.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var images = imagePost.defaultImages
    var indexPathPass = IndexPath(row: 0, section: 0)
    
    var posts:[Post] = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("View did load")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.backgroundColor =  colors.backgroundColor
        
        print("indexPathPass: \(self.indexPathPass.section)")
        DispatchQueue.main.async {
            print("indexPathPass inside: \(self.indexPathPass.section)")
            //let indexPath = IndexPath(row: 2, section: 0)
            self.tableView.scrollToRow(at: self.indexPathPass, at: .top, animated: false)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell
        
        // Configure the cell...
        
        cell.cellImageView.image = self.images[indexPath.row].image
        cell.cellImageView.image = cell.cellImageView.image?.resizeImageWith()
        self.images[indexPath.row].image = cell.cellImageView.image
        
        cell.backgroundColor =  UIColor(displayP3Red: 0, green: 255, blue: 1, alpha: 0.99)
        //cell.layer.shouldRasterize = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

//class SegueFromLeft: UIStoryboardSegue {
//    override func perform() {
//        let src = self.source
//        let dst = self.destination
//
//        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
//        dst.view.transform = CGAffineTransform(translationX: -src.view.frame.size.width, y: 0)
//
//        UIView.animate(withDuration: 0.25,
//                       delay: 0.0,
//                       options: .curveEaseInOut,
//                       animations: {
//                        dst.view.transform = CGAffineTransform(translationX: 0, y: 0)
//        },
//                       completion: nil { finished in
//                        src.present(dst, animated: false, completion: nil)
//        }
//        )
//    }
//}

extension UIViewController {
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false)
    }
}



