//
//  SearchFeedScreenViewController-LocationDelegate.swift
//  TileRecovery
//
//  Created by 123456 on 12/12/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import Firebase
import UIKit

protocol  MainLocationDelegate: class {
    func setLocation(location:GeoPoint)
}


