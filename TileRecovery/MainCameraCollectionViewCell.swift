//
//  MainCameraCollectionViewCell.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by Zach on 2/5/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

protocol gestureDelegate: class{
    func gestureDelegate()
}

protocol previewSegueDelegate: class {
    //func previewSegueDelegate(image:UIImage,device:AVCaptureDevice?)
    //func previewSegueDelegate()
    func previewSegueDelegate(image:UIImage?,device:AVCaptureDevice?,albumTypeSender:photoAlbumSender?,  cameraTypeSender:cameraSender?, asset:PHAsset?)
}

class MainCameraCollectionViewCell: UICollectionViewCell {
    
    weak var gdelegate: gestureDelegate?
    weak var pdelegate: previewSegueDelegate?
    
    
    @IBOutlet weak var myView: UIView!
    
    var captureSession = AVCaptureSession()
    private var sessionQueue: DispatchQueue!
    var captureConnection = AVCaptureConnection()
    
    var backCamera: AVCaptureDevice?
    var frontCamera: AVCaptureDevice?
    var currentCamera: AVCaptureDevice?
    
    var photoOutPut: AVCapturePhotoOutput?
    
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var image: UIImage?
    
    var usingFrontCamera = false
    
    let backgroundThreadHelper = DispatchQueue(label: "backgroundThreadHelper",qos:.userInteractive)
    
    @IBOutlet weak var shutterButton:UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var flipButton:UIButton!
    
    @objc func errorRecieved(){
        print("AVError recieved")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupCaptureSession()
        setupDevice()
        setupInput()
        setupPreviewLayer()
        
        backgroundThreadHelper.async {
            self.startRunningCaptureSession()
        }
        print("Inside of camera cell")
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(MainCameraCollectionViewCell.tapEdit(sender:)))
        addGestureRecognizer(pinchGesture)
        
        flashButton.setBackgroundImage(#imageLiteral(resourceName: "icons8-flash-auto-100"), for: .normal)
        flashButton.setTitle("", for: .normal)
        flipButton.setTitle("", for: .normal)
        flipButton.setBackgroundImage(#imageLiteral(resourceName: "icons8-switch-camera-100"), for: .normal)
        shutterButton.layer.borderWidth = 4
        shutterButton.layer.borderColor = colors.toolBarColor.cgColor
        shutterButton.backgroundColor? =  UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 0.25)
        
        //        NotificationCenter.default.addObserver(self,
        //                                               selector: #selector(self.setDefaultFocusAndExposure),
        //                                               name: NSNotification.Name.AVCaptureDeviceSubjectAreaDidChange,
        //                                               object: nil)
        self.dot = UIView(frame:CGRect(x: 0, y: 0, width: 0, height: 0))
        
        self.addSubview(self.dot)
    }
    
    //    deinit {
    //        NotificationCenter.default.removeObserver(self)
    //    }
    //
    //    @objc func setDefaultFocusAndExposure() {
    //
    //        if let device = AVCaptureDevice.default(for:AVMediaType.video) {
    //            do {
    //                try device.lockForConfiguration()
    //                device.isSubjectAreaChangeMonitoringEnabled = true
    //                device.focusMode = AVCaptureDevice.FocusMode.continuousAutoFocus
    //                device.exposureMode = AVCaptureDevice.ExposureMode.continuousAutoExposure
    //                device.unlockForConfiguration()
    //
    //            } catch {
    //                // Handle errors here
    //                print("There was an error focusing the device's camera")
    //            }
    //        }
    //    }
    
    
    
    @objc func tapEdit(sender: UIPinchGestureRecognizer){
        gdelegate?.gestureDelegate()
        guard let device = currentCamera else { return }
        
        if sender.state == .changed {
            
            let maxZoomFactor = device.activeFormat.videoMaxZoomFactor
            let pinchVelocityDividerFactor: CGFloat = 5.0
            
            do {
                
                try device.lockForConfiguration()
                defer { device.unlockForConfiguration() }
                
                let desiredZoomFactor = device.videoZoomFactor + atan2(sender.velocity, pinchVelocityDividerFactor)
                device.videoZoomFactor = max(1.0, min(desiredZoomFactor, maxZoomFactor))
                
            } catch {
                print(error)
            }
        }
        
    }
    func setupCaptureSession(){
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        sessionQueue = DispatchQueue(label: "session queue")
    }
    
    func setupDevice(usingFrontCamera:Bool = false){
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        
        for device in devices{
            if usingFrontCamera && device.position == AVCaptureDevice.Position.front {
                //backCamera = device
                self.currentCamera = device
                
            } else if device.position == AVCaptureDevice.Position.back {
                //frontCamera = device
                self.currentCamera = device            }
        }
    }
    
    
    
    
    
    
    //        }
    //        if usingFrontCamera {
    //        currentCamera = frontCamera
    //        }
    //        else{
    //            currentCamera = backCamera
    //        }
    
    //    func setupInput(){
    //        sessionQueue.async {
    //        do{
    //            let captureDeviceInput = try AVCaptureDeviceInput(device: self.currentCamera!)
    //            self.captureSession.addInput(captureDeviceInput)
    //            self.photoOutPut = AVCapturePhotoOutput()
    //            self.photoOutPut?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format:[AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
    //            self.captureSession.addOutput(self.photoOutPut!)
    //        }catch{
    //            print(error)
    //        }
    //    }
    //    }
    func setupInput() {
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: self.currentCamera!)
            if self.captureSession.canAddInput(captureDeviceInput) {
                self.captureSession.addInput(captureDeviceInput)
                print("input added")
            }
            self.photoOutPut = AVCapturePhotoOutput()
            self.photoOutPut?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format:[AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
            if self.captureSession.canAddOutput(self.photoOutPut!) {
                self.captureSession.addOutput(self.photoOutPut!)
                print("output added")
            }
        } catch {
            print(error)
        }
    }
    
    func setupPreviewLayer(){
        print("inside of setupPreviewLayer")
        print("setupPreviewLayer- The capture session is running: \(captureSession.isRunning)")
        cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
        cameraPreviewLayer?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.layer.insertSublayer(cameraPreviewLayer!, at: 0)
    }
    
    func startRunningCaptureSession(){
        captureSession.startRunning()
    }
    
    
    @IBAction func cameraButton_TouchUpInside(_ sender: Any) {
        print("Camera button tapped 2")
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        //
        settings.flashMode = self.flashSettings.flashMode
        settings.isAutoStillImageStabilizationEnabled = true
        if let photoOutputConnection = self.photoOutPut?.connection(with: .video){
            photoOutputConnection.videoOrientation = (cameraPreviewLayer?.connection?.videoOrientation)!
        }
        
        //
        photoOutPut?.capturePhoto(with: settings, delegate: self as AVCapturePhotoCaptureDelegate)
        //performSegue(withIdentifier: "showPhoto_segue", sender: nil)
        // print("camera button touched")
        //   pdelegate?.previewSegueDelegate(image: self.image!, device: currentCamera!)
        //pdelegate?.previewSegueDelegate()
    }
    
    //Pinch To Zoom in on Camera
    @IBAction func pinchToZoom(_ sender: UIPinchGestureRecognizer) {
        guard let device = currentCamera else { return }
        
        if sender.state == .changed {
            
            let maxZoomFactor = device.activeFormat.videoMaxZoomFactor
            let pinchVelocityDividerFactor: CGFloat = 5.0
            
            do {
                
                try device.lockForConfiguration()
                defer { device.unlockForConfiguration() }
                
                let desiredZoomFactor = device.videoZoomFactor + atan2(sender.velocity, pinchVelocityDividerFactor)
                device.videoZoomFactor = max(1.0, min(desiredZoomFactor, maxZoomFactor))
                
            } catch {
                print(error)
            }
        }
    }
    
    var dot:UIView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    
    //Tap To Focus Camera
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //print("In touches began")
        if !usingFrontCamera{
            let screenSize = UIScreen.main.bounds.size
            if let touchPoint = touches.first {
                let x = touchPoint.location(in: self).y / screenSize.height
                let y = 1.0 - touchPoint.location(in: self).x / screenSize.width
                let focusPoint = CGPoint(x: x, y: y)
                
                
                UIView.animate(withDuration: 0, animations: {
                    print("In animate: \(self.dot)")
                    self.dot.frame = CGRect(x:touchPoint.location(in: self).x-75/2 , y: touchPoint.location(in: self).y-75/2, width: 75, height: 75)
                    print("In animate: \(self.dot.frame)")
                    self.dot.backgroundColor = UIColor.clear
                    self.dot.layer.borderColor = colors.toolBarColor.cgColor
                    self.dot.layer.borderWidth = 2
                    self.dot.layer.cornerRadius = self.dot.frame.width/10
                    self.dot.isHidden = false
                    self.bringSubviewToFront(self.dot)
                })
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.dot.isHidden = true
                }
                
                if let device = currentCamera{
                    do {
                        try device.lockForConfiguration()
                        
                        device.focusPointOfInterest = focusPoint
                        //device.focusMode = .continuousAutoFocus
                        //device.focusMode = .autoFocus
                        device.focusMode = .continuousAutoFocus
                        //device.focusMode = .locked
                        device.exposurePointOfInterest = focusPoint
                        device.exposureMode = AVCaptureDevice.ExposureMode.continuousAutoExposure
                        device.unlockForConfiguration()
                    }
                    catch {
                        // just ignore
                    }
                }
            }
        }
        else{
            // print("tap to focus not enabled")
        }
    }
    
    //    var dot = UIView(frame:CGRect(x: 0, y: 0, width: 0, height: 0))
    //
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //
    //        let bounds = UIScreen.main.bounds
    //
    //        let touchPoint = touches.first! as UITouch
    //        let screenSize = bounds.size
    //        let focusPoint = CGPoint(x: touchPoint.location(in: self).y / screenSize.height, y: 1.0 - touchPoint.location(in: self).x / screenSize.width)
    //
    //        UIView.animate(withDuration: 0, animations: {
    //            self.dot.frame = CGRect(x:touchPoint.location(in: self).x, y: touchPoint.location(in: self).y, width: 75, height: 75)
    //            self.dot.backgroundColor = UIColor.clear
    //            self.dot.layer.borderColor = UIColor.red.cgColor
    //            self.dot.layer.borderWidth = 2
    //            self.dot.isHidden = false
    //            self.bringSubview(toFront: self.dot)
    //        })
    //
    //        self.dot.isHidden = true
    //
    //
    //        if let device = AVCaptureDevice.default(for:AVMediaType.video) {
    //            do {
    //                try device.lockForConfiguration()
    //                if device.isFocusPointOfInterestSupported {
    //                    device.focusPointOfInterest = focusPoint
    //                    device.focusMode = AVCaptureDevice.FocusMode.autoFocus
    //                }
    //                if device.isExposurePointOfInterestSupported {
    //                    device.exposurePointOfInterest = focusPoint
    //                    device.exposureMode = AVCaptureDevice.ExposureMode.autoExpose
    //                }
    //                device.unlockForConfiguration()
    //
    //            } catch {
    //                // Handle errors here
    //                print("There was an error focusing the device's camera")
    //            }
    //        }
    //    }
    
    
    
    @IBAction func FlipThe_camera(_ sender: UIButton?) {
        print("Flip Touched")
        
        self.captureSession.beginConfiguration()
        if let inputs = self.captureSession.inputs as? [AVCaptureDeviceInput] {
            for input in inputs {
                self.captureSession.removeInput(input)
                print("input removed")
            }
            //This seemed to have fixed it
            for output in self.captureSession.outputs{
                captureSession.removeOutput(output)
                print("out put removed")
            }
        }
        
        self.usingFrontCamera = !self.usingFrontCamera
        self.setupCaptureSession()
        self.setupDevice(usingFrontCamera: self.usingFrontCamera)
        self.setupInput()
        self.captureSession.commitConfiguration()
        self.startRunningCaptureSession()
    }
    
    
    
    
    var numberOfTaps:Int = 0
    
    var flashSettings = AVCapturePhotoSettings()
    
    //Change Flash Button Depending on state here
    
    @IBAction func toggleFlash(_ sender: UIButton){
        numberOfTaps += 1
        print("number of taps: \(numberOfTaps)")
        let whichMode:Int = numberOfTaps%3
        print("whichMode: \(whichMode)")
        switch whichMode {
        case 0:
            self.flashSettings.flashMode = .auto
            //self.flashButton.setTitle("auto", for: .normal)
            flashButton.setBackgroundImage(#imageLiteral(resourceName: "icons8-flash-auto-100"), for: .normal)
            print("flash mode auto")
        case 1:
            self.flashSettings.flashMode = .off
            //self.flashButton.setTitle("off", for: .normal)
            flashButton.setBackgroundImage(#imageLiteral(resourceName: "icons8-flash-off-100"), for: .normal)
            print("flash mode off")
        case 2:
            self.flashSettings.flashMode = .on
            //  self.flashButton.setTitle("on", for: .normal)
            flashButton.setBackgroundImage(#imageLiteral(resourceName: "icons8-flash-on-100"), for: .normal)
            print("flash mode on")
        default:
            self.flashSettings.flashMode = .auto
            flashButton.setBackgroundImage(#imageLiteral(resourceName: "icons8-flash-auto-100"), for: .normal)
            break
        }
    }
    
    //Flip to front and back camera
    //    @IBAction func FlipThe_camera(_ sender: UIButton) {
    //        print("Flip touched")
    //        do{
    //            captureSession.beginConfiguration()
    //            let oldDeviceInput = try AVCaptureDeviceInput(device: backCamera!)
    //            captureSession.removeInput(oldDeviceInput)
    //            if let inputs = captureSession.inputs as? [AVCaptureDeviceInput] { for input in inputs { captureSession.removeInput(input) } }
    //            let captureDeviceInput = try AVCaptureDeviceInput(device: frontCamera!)
    //           // let captureconnectioninput = try AVCaptureConnection(inputPort: captureDeviceInput.ports, videoPreviewLayer: cameraPreviewLayer)
    //
    //            captureSession.addInput(captureDeviceInput)
    //            photoOutPut = AVCapturePhotoOutput()
    //            photoOutPut?.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format:[AVVideoCodecKey: AVVideoCodecType.jpeg])], completionHandler: nil)
    //            captureSession.addOutput(photoOutPut!)
    //            captureSession.commitConfiguration()
    //            setupPreviewLayer()
    //        }catch{
    //            print(error)
    //        }    }
    
    
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //        if segue.identifier == "showPhoto_segue"{
    //            let previewVC = segue.destination as! PreviewController
    //            if currentCamera == frontCamera{
    //                self.image = UIImage(cgImage: (self.image?.cgImage!)!, scale: (self.image?.scale)!, orientation: .leftMirrored)
    //            }
    //
    //            previewVC.image = self.image
    //            if previewVC.image == nil {
    //                print("no image before segue")
    //                return
    //            }
    //        }
    //    }
    
}


extension MainCameraCollectionViewCell: AVCapturePhotoCaptureDelegate{
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        print("Im in the photo out put delgate")
        if let imageData = photo.fileDataRepresentation(){
            print(imageData)
            image = UIImage(data: imageData)
            //   performSegue(withIdentifier: "showPhoto_segue", sender: nil)
            if(self.image == nil){
                print("The image is empty")
            }
            print("The capture session is running photoOutput: \(captureSession.isRunning)")
            print("The capture session has been interupted photoOutput: \(captureSession.isInterrupted)")
            
            pdelegate?.previewSegueDelegate(image: self.image!, device: currentCamera!,albumTypeSender:  nil, cameraTypeSender:nil, asset: nil)
        }
        else{
            print("The photoOutput delegate error is \(String(describing: error))")
        }
    }
    
    
    func photoOutput(_ output: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        print("The photo is about to be taken")
    }
    
}



