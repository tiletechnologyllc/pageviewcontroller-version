//
//  selectedCell-Delegate.swift
//  TileRecovery
//
//  Created by 123456 on 1/16/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol SelectedCellDelegate: class {
    func setSelectedCell(selectedUser:User?,selectedTag:Tag?)
}

