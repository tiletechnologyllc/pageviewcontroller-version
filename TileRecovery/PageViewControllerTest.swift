//
//  PageViewControllerTest.swift
//  TileRecovery
//
//  Created by 123456 on 3/30/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol PageViewControllerDelegate:class {
    func addSegmentedViewController(userID:String)
    func removeSegmentedViewController()
}

var tileColor:UIColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0)
var backgroundColor:UIColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)

class PageViewControllerTest:UIPageViewController,UIPageViewControllerDelegate,UIPageViewControllerDataSource{
  
    var pages:[UIViewController] = [UIViewController]()
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        let mainVC:segmentedViewController = segmentedViewController()
        mainVC.pageViewControllerDelegate = self
        mainVC.userID = User.signedInUserID
        mainVC.getFollowing()
        mainVC.getUsernameProfilePicture()
        pages.append(mainVC)
        self.setViewControllers([mainVC], direction: .forward, animated: true) { (completed) in
            print("completed adding in view controllers:\(completed)")
        }
        //pages.append(segmentedViewController())
    }
    
    func prepareSegmentedView(viewController:segmentedViewController,userID:String = User.signedInUserID){
        viewController.userID = userID
        viewController.getFollowing()
        viewController.getUsernameProfilePicture()
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else{
            return nil
        }
        
        if viewControllerIndex > 0{
            let vc = self.pages[viewControllerIndex - 1]
//            vc.view.backgroundColor = UIColor.green
            return vc
        }else{
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.index(of: viewController) else{
            return nil
        }
        
//        pages.append(UIViewController())
        
        if viewControllerIndex < pages.count - 1{
            let vc = self.pages[viewControllerIndex + 1]
            return vc
        }else{
            return nil
        }
    }
}

extension PageViewControllerTest:PageViewControllerDelegate{
    func addSegmentedViewController(userID:String = User.signedInUserID) {
        let VC = segmentedViewController()
        VC.pageViewControllerDelegate = self
        VC.userID = userID
        VC.getFollowing()
        VC.getUsernameProfilePicture()
        self.pages.append(VC)
        self.setViewControllers([pages[pages.count - 1]], direction: .forward, animated: true, completion: nil)
    }
    
    func removeSegmentedViewController(){
        if pages.count > 1{
        self.pages.remove(at: pages.count - 1)
        self.setViewControllers([pages[pages.count - 1]], direction: .reverse, animated: true, completion: nil)
        }
        else{
            print("Back to root page")
        }
    }
    
    
}
