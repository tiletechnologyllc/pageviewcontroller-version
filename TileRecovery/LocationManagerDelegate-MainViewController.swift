//
//  LocationManagerDelegate-MainViewController.swift
//  TileRecovery
//
//  Created by 123456 on 1/8/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit
import Firebase



extension AppDelegate:CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed: \(error.localizedDescription) ")
    }
    func locationManager(_ manager: CLLocationManager,didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied:
            print("Access denied")
            break
            
        case .authorizedWhenInUse:
            print("Access Level: when in use")
            enableBasicLocationServices()
            //locationManager.startUpdatingLocation()
            break
            
        case .notDetermined, .authorizedAlways:
            print("Access Level: Always, not determined")
            break
        }
    }
    
    func enableBasicLocationServices() {
        
        self.locationManager.requestLocation()
        self.locationManager.distanceFilter = 10
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            self.locationManager.requestAlwaysAuthorization()
            //self.locationManager.requestWhenInUseAuthorization()
            break
            
        case .restricted, .denied:
            // Disable location features
            // disableMyLocationBasedFeatures()
            print("Location Services are not enabled")
            break
            
        case .authorizedWhenInUse:
            print("When in use is enabled")
            
            //locationManager.requestLocation()
            locationManager.startUpdatingLocation()
            break
            
        case  .authorizedAlways:
            // Enable location features
            // enableMyWhenInUseFeatures()
            print("Always in use enabled")
            //locationManager.delegate = self
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.pausesLocationUpdatesAutomatically = true
            break
        }
    }
    
    
    //    func startReceivingSignificantLocationChanges() {
    //        let authorizationStatus = CLLocationManager.authorizationStatus()
    //        if authorizationStatus != .authorizedAlways || authorizationStatus != .authorizedWhenInUse{
    //            // User has not authorized access to location information.
    //            print("User has not authorized access to location information")
    //            locationManager.requestAlwaysAuthorization()
    //
    //            return
    //        }
    //
    //        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
    //            // The service is not available.
    //            print("Significant location change monitoring is not availble")
    //            return
    //        }
    //        locationManager.delegate = self
    //       // locationManager.startMonitoringSignificantLocationChanges()
    //
    //        locationManager.pausesLocationUpdatesAutomatically = true
    //    }
    
    func locationManager(_ manager: CLLocationManager,  didUpdateLocations locations: [CLLocation]) {
        if locations.count > 0{
            self.locationManager.stopUpdatingLocation()
        }
        if let lastLocation = locations.last, !self.didFindLocation{
            
            print("didUpdate Location Calledd")
            print("last location lat: \(lastLocation.coordinate.latitude), long: \(lastLocation.coordinate.longitude)")
            
            // Do something with the location.
            let userLocation:GeoPoint = GeoPoint(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude)
            print("Get the datas called")
            //Call Firebase Function here
            //        getNearByRestaurants(center: userLocation, completiionHandler: {
            //            print("Function Ran")
            //        })
            if let locationDelegate = self.locationDelegate{
            locationDelegate.setLocation(location: userLocation)
            }else{
                print("Location Delegate has not yet set")
            }
            self.userLocation = userLocation
            didFindLocation = true
        }
        self.locationManager.stopUpdatingLocation()
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            // Location updates are not authorized.
            manager.stopMonitoringSignificantLocationChanges()
            return
        }
        // Notify the user of any errors.
        print("Location Manager Error: \(error.localizedDescription)")
    }
}
