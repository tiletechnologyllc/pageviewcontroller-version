//
//  User.swift
//  TileRecovery
//
//  Created by 123456 on 1/11/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import Firebase
import UIKit

class User{
    var userID:String
    var username:String
    var photoURL:String
    var storagePath:String{
        return "\(self.userID).jpeg"
    }
    
    static var signedInUserID:String!
    static var signedInUser:User?
    
    static var defaultProfilePhotoURL:String = "https://storage.googleapis.com/pullupanddownfirebase.appspot.com/Profile%20Pictures%2F8EyBxZTKgGPFU63W8sWu2Z8NhEl2.jpeg?GoogleAccessId=firebase-adminsdk-ma36y%40pullupanddownfirebase.iam.gserviceaccount.com&Expires=17315190361&Signature=MJ2qWCkqNrThCH%2FOe8mM1Y3Bm1Nbiy0%2ByGj%2F%2FDD9UjbIItj4EcrQkYNIez2IG4REwN%2FGInpWAd0voS6uZ04UeuNk3FB1Fi87rwtKr8BRNxoFxfIHNe%2ByCtU2AYbOLK6nBVWg6TFH8OH7ZVE0C9Rf%2Fzo3Kc9mUOGVXG0v%2FnThjZqvHWlddaEln9K6JehqkhCCqhq2X9jKWAKvtEZM69600buKqjSgi0U0%2FIKC7m%2BSth1ufeRlpCWjbbyWJU6siAMGkxIF6e57avfmZRBSmMzL2Ubpn0lY00Nv%2BJr8kYVx9OASL79OsDVoRMqWN%2F51b14Re%2BkRuo%2FAo9O6bK84dVCXNw%3D%3D"
    
    init(userID:String,username:String,photoURL:String) {
        self.userID = userID
        self.username = username
        self.photoURL = photoURL
    }
    
    init?(snapshot:DataSnapshot){
        print("snapshot.value: \(snapshot.value as? [String:Any])")
        guard let data = snapshot.value as? [String:Any], let username = data["username"] as? String,
            let photoURL = data["photoURL"] as? String else{
            print("Failed to initalize user from data snapshot")
            return nil
        }
        self.userID = snapshot.key
        self.username = username
        self.photoURL = photoURL
    }
    

    //Come back and adjust caches later no need for 3 following caches adjust it down to one 
    static var signedInUserFollowingCache = NSCache<NSString,AnyObject>()
    
//    static var allFollowingCache = NSCache<NSString,NSArray>()
    
//    static var followingCache = NSCache<NSString,User>()
    
    static var followersCache = NSCache<NSString,User>()
    
    static var PostsCache = NSCache<NSString,Post>()
    
    static var UsersCache = NSCache<NSString,User>()
    
    static func checkForFollowing(followID:String, completion: ((_ isFollowing:Bool)->Void)?){
        Database.database().reference(withPath: "Following/\(User.signedInUserID!)/tagsUsers/\(followID)").observeSingleEvent(of: .value, with: {(snapshot) in
            if let completion = completion{
                print("Snapshot.exists(): \(snapshot.exists())")
                print("search path: \(snapshot.ref)")
                completion(snapshot.exists())
            }
        }, withCancel: {(error) in
            print("Error checking for following: \(error)")
        })
    }
    
    static func updateProfilePictureInCache(imageURL:String, userID:String = User.signedInUserID){
        if var user = UsersCache.object(forKey: userID as NSString){
            user.photoURL = imageURL
            UsersCache.setObject(user, forKey: userID as NSString)
        }
    }
}
