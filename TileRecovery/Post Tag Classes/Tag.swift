//
//  Tag.swift
//  TileRecovery
//
//  Created by 123456 on 1/8/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import Firebase

class Tag:Codable {
    
    let name:String
    var location:GeoPoint {
        return GeoPoint(latitude: latitude, longitude: longitude)
    }
    let longitude:Double
    let latitude:Double
    let tagID:String
    let storageReference:StorageReference? = nil
    let storageRef:String
    let photoURL:String
    
    var describing:String{
        return "name: \(name), location: \(location), tagID: \(tagID)"
    }
    
    static var tagsCache:NSCache = NSCache<NSString,Tag>()
    
    enum CodingKeys: String,CodingKey{
        case name
        case tagID
        case longitude
        case latitude
        case storageRef
        case photoURL
    }
    
    init?(name:String,location:[String:Any],tagID:String,storageRef:String,photoURL:String){
        self.name = name
        self.tagID = tagID
        guard let latitude = location["latitude"] as? Double, let longitude = location["longitude"] as? Double else{
            print("An error occured parsing longitude and latitude")
            return nil
        }
        self.latitude = latitude
        self.longitude = longitude
        self.storageRef = storageRef
        self.photoURL = photoURL
    }
    
    required init(from decoder: Decoder) throws {
        let valueContatiner = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try valueContatiner.decode(String.self, forKey: CodingKeys.name)
        self.tagID = try valueContatiner.decode(String.self, forKey: CodingKeys.tagID)
        self.longitude = try valueContatiner.decode(Double.self, forKey: CodingKeys.longitude)
        self.latitude = try valueContatiner.decode(Double.self, forKey: CodingKeys.latitude)
        self.storageRef = try valueContatiner.decode(String.self, forKey: CodingKeys.storageRef)
        self.photoURL = try valueContatiner.decode(String.self, forKey: CodingKeys.photoURL)
    }
    
    init?(name:String,location:GeoPoint,tagID:String,storageRef:String,photoURL:String){
        self.name = name
        self.latitude = location.latitude
        self.longitude = location.longitude
        self.tagID = tagID
        self.storageRef = storageRef
        self.photoURL = photoURL
    }
    
}
