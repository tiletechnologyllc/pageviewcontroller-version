//
//  blankButtonCell.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by 123456 on 8/21/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//Zach is somewhat cool

import Foundation
import UIKit

class blankButtonCell:UICollectionViewCell{
    
    var cellChangeButton:UIButton!
    weak var addOrRemoveCellDelegate:addOrRemoveCell?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        cellChangeButton = UIButton(frame: CGRect(x: 0, y:self.frame.midY , width: self.frame.width, height: self.frame.height/8))
        
        self.addSubview(cellChangeButton)
        cellChangeButton.backgroundColor = .red
        cellChangeButton.setTitle("remove screen", for: .normal)
        
        cellChangeButton.addTarget(self, action: #selector(cellChangeButton_tapped), for: .touchUpInside)
        
        cellChangeButton.translatesAutoresizingMaskIntoConstraints = false
        cellChangeButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        cellChangeButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
    }
    
    var removeCell = false
    
    @objc func cellChangeButton_tapped(){
        print("Cell change button tapped")
        
        if removeCell{
            addOrRemoveCellDelegate?.removeCell(sender: self, cellType: nil)
            removeCell = !removeCell
            self.cellChangeButton.setTitle("Add Cell", for: .normal)
        }
        else{
            addOrRemoveCellDelegate?.addCell(sender: self, cellType: nil)
            removeCell = !removeCell
            self.cellChangeButton.setTitle("Remove Cell", for: .normal)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol addOrRemoveCell: class{
    func removeCell(sender:Any?,cellType:CellTypes?)
    func addCell(sender:Any?,cellType:CellTypes?)
    func addCell(sender:Any?,user:User )
    func addCell(sender:Any?,tag:Tag)
}


extension MainViewController:addOrRemoveCell{
  
    func addCell(sender: Any?, user: User) {
        print("add cell for user called")
        self.selectedUser = user
        
        self.selectedTag = nil
        let cellModel = RowModel(type: .FollowersProfileScreenCell, title: "FollowersProfileScreenCell")
        
        var nextIndex:Int = 0
        print("sender: \(sender), user: \(user.userID)")
        
        if sender as? SearchFeedScreenViewController != nil{
            nextIndex = 1
            print("sender was searchFeedScreenViewController")
        }else if sender as? segmentedViewController != nil{
           
            let indexOfLastFollowersScreen = collectionViewManager.cells.lastIndex(where: {(cell) in
                return cell.type == CellTypes.FollowersProfileScreenCell
            })
            
            if let indexOfLastFollowersScreen = indexOfLastFollowersScreen {
                nextIndex = indexOfLastFollowersScreen + 1
            }else{
                
                let indexOfProfileScreen = collectionViewManager.cells.firstIndex(where: {(cell) in
                    return cell.type == CellTypes.MainProfileScreenCell
                })
                
                if let indexOfProfileScreen = indexOfProfileScreen{
                    nextIndex = indexOfProfileScreen + 1
                }
            }
        }else if sender as? FollowersProfileScreenCell != nil{
            let indexOfLastFollowersScreen = collectionViewManager.cells.lastIndex(where: {(cell) in
                return cell.type == CellTypes.FollowersProfileScreenCell
            })
            
            if let indexOfLastFollowersScreen = indexOfLastFollowersScreen {
                nextIndex = indexOfLastFollowersScreen + 1
            }else{
                
                let indexOfProfileScreen = collectionViewManager.cells.firstIndex(where: {(cell) in
                    return cell.type == CellTypes.MainProfileScreenCell
                })
                
                if let indexOfProfileScreen = indexOfProfileScreen{
                    nextIndex = indexOfProfileScreen + 1
                }
            }
        }else if sender as? MainProfileScreenCell != nil{
            let indexOfLastFollowersScreen = collectionViewManager.cells.lastIndex(where: {(cell) in
                return cell.type == CellTypes.FollowersProfileScreenCell
            })
            
            if let indexOfLastFollowersScreen = indexOfLastFollowersScreen {
                nextIndex = indexOfLastFollowersScreen + 1
            }else{
                
                let indexOfProfileScreen = collectionViewManager.cells.firstIndex(where: {(cell) in
                    return cell.type == CellTypes.MainProfileScreenCell
                })
                
                if let indexOfProfileScreen = indexOfProfileScreen{
                    nextIndex = indexOfProfileScreen + 1
                }
            }
        }
        let indexPath = IndexPath(item:nextIndex,section:0)
        
        if let selectedUser = selectedUser{
            self.selectedUsersDict[indexPath] = selectedUser
        }
        
        for cell in self.collectionViewManager.cells{
            print("Cell in add cell user: \(cell.type)")
        }
        print("next index: \(nextIndex)")
        self.MainCollectionView.performBatchUpdates({
            self.collectionViewManager.cells.insert(cellModel, at: nextIndex)
            self.MainCollectionView.insertItems(at: [indexPath])
            
        }) { (completed) in
            print("batch updates completed: \(completed) in addCell user")
            self.MainCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
            self.previousPageSwipe = nextIndex
        }
    }
    
    func addCell(sender: Any?, tag: Tag) {
        
        print("add cell for Tag called")
        
        self.selectedTag = tag
        self.selectedUser = nil
        
        let cellModel = RowModel(type: .MainFeedCollectionViewCell, title: "MainFeedCollectionViewCell")
        
        var nextIndex:Int = 0
        print("sender: \(sender), tag: \(tag.tagID)")
        
        if sender as? SearchFeedScreenViewController != nil{
            nextIndex = 1
            print("sender was searchFeedScreenViewController")
        }else if sender as? segmentedViewController != nil{
            
            let indexOfLastFollowersScreen = collectionViewManager.cells.lastIndex(where: {(cell) in
                return cell.type == CellTypes.FollowersProfileScreenCell
            })
            
            if let indexOfLastFollowersScreen = indexOfLastFollowersScreen {
                nextIndex = indexOfLastFollowersScreen + 1
            }else{
                
                let indexOfProfileScreen = collectionViewManager.cells.firstIndex(where: {(cell) in
                    return cell.type == CellTypes.MainProfileScreenCell
                })
                
                if let indexOfProfileScreen = indexOfProfileScreen{
                    nextIndex = indexOfProfileScreen + 1
                }
            }
        }
        let indexPath = IndexPath(item:nextIndex,section:0)
        
        if let selectedUser = selectedUser{
            self.selectedUsersDict[indexPath] = selectedUser
        }
        
        for cell in self.collectionViewManager.cells{
            print("Cell in add cell user: \(cell.type)")
        }
        print("next index: \(nextIndex)")
        self.MainCollectionView.performBatchUpdates({
            self.collectionViewManager.cells.insert(cellModel, at: nextIndex)
            self.MainCollectionView.insertItems(at: [indexPath])
            
        }) { [weak self] (completed) in
            guard let self = self else{
                return
            }
            print("batch updates completed: \(completed) in addCell user")
            self.MainCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
            self.previousPageSwipe = nextIndex
        }
    }
    

    
    
    func addCell(sender:Any?,cellType:CellTypes?) {
        print("Made it to add cells")
        print("add cell has been called sender: \(sender), cellType: \(cellType)")
       
        
        if let searchFeedScreenViewController = sender as? SearchFeedScreenViewController{
            if let selectedUser = searchFeedScreenViewController.selectedUser{
                self.selectedUser = selectedUser
                self.selectedTag = nil
                self.collectionViewManager.cells.insert(RowModel(type: .FollowersProfileScreenCell, title: "FollowersProfileScreenCell"), at: 1)
            }else if let selectedTag = searchFeedScreenViewController.selectedTag{
                self.selectedTag = selectedTag
                self.selectedUser = nil
                self.collectionViewManager.cells.insert(RowModel(type: .MainFeedCollectionViewCell, title: "MainFeedCollectionViewCell"), at: 1)
            }
        }else if let segmentedViewController = sender as? segmentedViewController {
            
        }else if let mainFeedCollectionViewCell = sender as? MainFeedCollectionViewCell{
            
        }
        
        let indexOfProfileScreen = collectionViewManager.cells.firstIndex(where: {(cell) in
            return cell.type == CellTypes.MainProfileScreenCell
        })
        let indexOfLastFollowersScreen = collectionViewManager.cells.lastIndex(where: {(cell) in
            return cell.type == CellTypes.FollowersProfileScreenCell
        })
        var nextIndex:Int = 0
        print("Collection Cells before: ")
        for cell in collectionViewManager.cells{
            print(cell.type)
        }
        print("indexOfProfileScreen before: \(indexOfProfileScreen)")
        print("indexOfLastFollowersScreen: \(indexOfLastFollowersScreen)")
        if let indexOfProfileScreen = indexOfProfileScreen{
            for i in 0..<collectionViewManager.cells.count{
                if(collectionViewManager.cells[i].type == CellTypes.FollowersProfileScreenCell && i < indexOfProfileScreen){
                    
                    print("collectionViewManager.cells[i].type == CellTypes.FollowersProfileScreenCell: \(collectionViewManager.cells[i].type == CellTypes.FollowersProfileScreenCell)")
                    
                    if let indexOfLastFollowersScreen = indexOfLastFollowersScreen{
                        nextIndex = indexOfLastFollowersScreen + 1
                    }else{
                        nextIndex = i + 1
                    }
                    print("indexOfProfileScreen: \(indexOfProfileScreen)")
                    print("indexOfLastFollowersScreen: \(indexOfLastFollowersScreen)")
                    print("i: \(i)")
                    print("next index: \(nextIndex)")
                    print("Collection Cells before insert: ")
                    break
                }else if(collectionViewManager.cells[i].type == CellTypes.FollowersProfileScreenCell && i > indexOfProfileScreen){
                    if let indexOfLastFollowersScreen = indexOfLastFollowersScreen{
                        nextIndex = indexOfLastFollowersScreen + 1
                    }else{
                        nextIndex = collectionViewManager.cells.count
                    }
                    print("next Index: \(nextIndex)")
                    
                    break
                }else if(collectionViewManager.cells[i].type == CellTypes.MainProfileScreenCell && i == collectionViewManager.cells.count - 1){
                    nextIndex = indexOfProfileScreen + 1//collectionViewManager.cells.count
                }
            }
            
            MainCollectionView.performBatchUpdates({[weak self]() in
                guard let self = self else{
                    return
                }
                for cell in collectionViewManager.cells{
                    print(cell.type)
                }
                self.collectionViewManager.cells.insert(RowModel(type: .FollowersProfileScreenCell, title: "FollowersProfileScreenCell"), at: nextIndex)//append(RowModel(type: .FollowersProfileScreenCell, title: "FollowersProfileScreenCell"))
                print("number of items in cell \(collectionViewManager.cells.count)")
                self.MainCollectionView.insertItems(at: [IndexPath(item:nextIndex,section:0)])
            }, completion: {[weak self] (completed) in
                guard let self = self else{
                    return
                }
                print("Completed performing add batch updates completed: \(completed)")
                self.MainCollectionView.scrollToItem(at: IndexPath(item:nextIndex,section:0), at: .left, animated: true)
                print("Collection Cells after: ")
                for cell in self.collectionViewManager.cells{
                    print(cell.type)
                }
            })

        /*
        if(sender as? segmentedViewController != nil){
            
        }else if (sender as? SearchFeedScreenViewController != nil){
            if let selectedUser = sender as? User{
                self.selectedUser = selectedUser
                self.selectedTag = nil
                self.collectionViewManager.cells.insert(RowModel(type: .FollowersProfileScreenCell, title: "FollowersProfileScreenCell"), at: 1)
            }else{
                self.selectedUser = nil
                self.collectionViewManager.cells.insert(RowModel(type: .MainFeedCollectionViewCell, title: "MainFeedScreenCollectionViewCell"), at: 1)
            }
            
            self.MainCollectionView.performBatchUpdates({
                self.MainCollectionView.insertItems(at: [IndexPath(item: 1, section: 0)])
            }, completion: {(completed) in
                print("Batch updates completed for the search feed screen")
                self.shouldRemoveFollowersProfileScreen = true
                self.MainCollectionView.scrollToItem(at: IndexPath(item: 1,section:0), at: .left, animated: true)
            })
            
        }else if (sender as? MainFeedCollectionViewCell != nil){
            
        }
        
        if (sender as? SearchFeedScreenViewController != nil) && (cellType == CellTypes.MainSearchFeedScreenCell){
            print("The sender was the searchFeedScreen")
            self.collectionViewManager.cells.insert(RowModel(type: .MainFeedCollectionViewCell, title: "MainFeedScreenCollectionViewCell"), at: 1)
            self.MainCollectionView.reloadData()
            self.MainCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: true)
        }else if let selectedUser = sender as? User, (cellType == CellTypes.MainSearchFeedScreenCell){
            self.selectedUser = selectedUser
            self.selectedTag = nil
            MainCollectionView.performBatchUpdates({() in
                self.collectionViewManager.cells.insert(RowModel(type: .FollowersProfileScreenCell, title: "FollowersProfileScreenCell"), at: 1)
                print("number of items in cell \(collectionViewManager.cells.count)")
                self.MainCollectionView.insertItems(at: [IndexPath(item: 1 ,section:0)])
            }, completion: {(completed) in
                print("Completed performing add batch updates completed: \(completed)")
                self.shouldRemoveFollowersProfileScreen = true
                self.MainCollectionView.scrollToItem(at: IndexPath(item: 1,section:0), at: .left, animated: true)
            })
        }
        else if (sender as? segmentedViewController != nil) && (cellType == .MainFeedCollectionViewCell){
            print("The sender was the profile screen")
            
            self.MainCollectionView.performBatchUpdates({
                self.collectionViewManager.cells.insert(RowModel(type: .MainFeedCollectionViewCell, title: "MainFeedScreenCollectionViewCell"), at: collectionViewManager.cells.count)
                self.MainCollectionView.insertItems(at: [IndexPath(item: self.collectionViewManager.cells.count - 1, section: 0)])
            }, completion: {(completed) in
                        self.MainCollectionView.scrollToItem(at: IndexPath(item: self.collectionViewManager.cells.count - 1, section: 0), at: .right, animated: true)
            })

    
        }
        else if (sender as? segmentedViewController != nil) && (cellType == .FollowersProfileScreenCell){
            print("The sender was the followers profile screen")
            //           self.collectionViewManager.cells.append(RowModel(type: .FriendsProfileScreenCell, title: "FriendsProfileScreenCell"))
            
            //            self.MainCollectionView.reloadData()
            //            //self.MainCollectionView.scrollToItem(at: IndexPath(item: collectionViewManager.cells.count - 1, section: 0), at: .right, animated: true)
            //            self.profileScreenSegmentedViewController.collectionView.isUserInteractionEnabled = true
            //            //self.friendsProfileScreenSegmentedViewController.collectionView.isUserInteractionEnabled = true
            
            let indexOfProfileScreen = collectionViewManager.cells.firstIndex(where: {(cell) in
                return cell.type == CellTypes.MainProfileScreenCell
            })
            let indexOfLastFollowersScreen = collectionViewManager.cells.lastIndex(where: {(cell) in
                return cell.type == CellTypes.FollowersProfileScreenCell
            })
            var nextIndex:Int = 0
            print("Collection Cells before: ")
            for cell in collectionViewManager.cells{
                print(cell.type)
            }
            print("indexOfProfileScreen before: \(indexOfProfileScreen)")
            print("indexOfLastFollowersScreen: \(indexOfLastFollowersScreen)")
            if let indexOfProfileScreen = indexOfProfileScreen{
                for i in 0..<collectionViewManager.cells.count{
                    if(collectionViewManager.cells[i].type == CellTypes.FollowersProfileScreenCell && i < indexOfProfileScreen){
                        
                        print("collectionViewManager.cells[i].type == CellTypes.FollowersProfileScreenCell: \(collectionViewManager.cells[i].type == CellTypes.FollowersProfileScreenCell)")
                          
                            if let indexOfLastFollowersScreen = indexOfLastFollowersScreen{
                                nextIndex = indexOfLastFollowersScreen + 1
                            }else{
                                nextIndex = i + 1
                            }
                            print("indexOfProfileScreen: \(indexOfProfileScreen)")
                            print("indexOfLastFollowersScreen: \(indexOfLastFollowersScreen)")
                            print("i: \(i)")
                            print("next index: \(nextIndex)")
                            print("Collection Cells before insert: ")
                            break
                    }else if(collectionViewManager.cells[i].type == CellTypes.FollowersProfileScreenCell && i > indexOfProfileScreen){
                        if let indexOfLastFollowersScreen = indexOfLastFollowersScreen{
                        nextIndex = indexOfLastFollowersScreen + 1
                        }else{
                            nextIndex = collectionViewManager.cells.count
                        }
                        print("next Index: \(nextIndex)")

                        break
                    }else if(collectionViewManager.cells[i].type == CellTypes.MainProfileScreenCell && i == collectionViewManager.cells.count - 1){
                        nextIndex = indexOfProfileScreen + 1//collectionViewManager.cells.count
                    }
                }
                
                MainCollectionView.performBatchUpdates({() in
                    for cell in collectionViewManager.cells{
                        print(cell.type)
                    }
                    self.collectionViewManager.cells.insert(RowModel(type: .FollowersProfileScreenCell, title: "FollowersProfileScreenCell"), at: nextIndex)//append(RowModel(type: .FollowersProfileScreenCell, title: "FollowersProfileScreenCell"))
                    print("number of items in cell \(collectionViewManager.cells.count)")
                    self.MainCollectionView.insertItems(at: [IndexPath(item:nextIndex,section:0)])
                }, completion: {(completed) in
                    print("Completed performing add batch updates completed: \(completed)")
                    self.MainCollectionView.scrollToItem(at: IndexPath(item:nextIndex,section:0), at: .left, animated: true)
                    print("Collection Cells after: ")
                    for cell in self.collectionViewManager.cells{
                        print(cell.type)
                    }
                })
            }
            */
            
//            MainCollectionView.performBatchUpdates({() in
//                self.collectionViewManager.cells.append(RowModel(type: .FollowersProfileScreenCell, title: "FollowersProfileScreenCell"))
//                print("number of items in cell \(collectionViewManager.cells.count)")
//                self.MainCollectionView.insertItems(at: [IndexPath(item:collectionViewManager.cells.count - 1 ,section:0)])
//            }, completion: {(completed) in
//                print("Completed performing add batch updates completed: \(completed)")
//                self.MainCollectionView.scrollToItem(at: IndexPath(item:self.collectionViewManager.cells.count - 1,section:0), at: .left, animated: true)
//            })

            //self.MainCollectionView.scrollToItem(at: IndexPath(item:collectionViewManager.cells.count - 1,section:0), at: .left, animated: true)
        }
    }
    
    
    
    func removeCell(sender:Any?,cellType:CellTypes?) {
        print("remove cell called")
//        if let profileScreen = self.profileScreenSegmentedViewController{
//            if let profileScreenCollectionView = profileScreen.collectionView{
//                profileScreen.collectionView.isUserInteractionEnabled = true
//            }
//        }
//        if  sender as? SearchFeedScreenViewController != nil{
//            self.collectionViewManager.cells.remove(at: 1)
//            print("remove at spot 1")
//            self.MainCollectionView.reloadData()
//            //self.MainCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: false)
//        }
//        else if sender as? segmentedViewController != nil{
//            print("cell to be removed: \(collectionViewManager.cells.count - 1)")
//            self.collectionViewManager.cells.remove(at:  collectionViewManager.cells.count - 1)
//            self.profileScreenSegmentedViewController.collectionView.isUserInteractionEnabled = true
//            //self.MainCollectionView.deleteItems(at: [IndexPath(item: collectionViewManager.cells.count - 1, section: 0)])
//            //self.MainCollectionView.scrollToItem(at: IndexPath(item: collectionViewManager.cells.count - 1, section: 0), at: .left, animated: true)
//            self.MainCollectionView.reloadData()
//            self.MainCollectionView.scrollToItem(at: IndexPath(item: collectionViewManager.cells.count - 1, section: 0), at: .left, animated: true)
//        }
        
//                if let profileScreen = self.profileScreenSegmentedViewController{
//                    if let profileScreenCollectionView = profileScreen.collectionView{
//                        profileScreen.collectionView.isUserInteractionEnabled = true
//                    }
//        }
//
//        
//                if  sender as? SearchFeedScreenViewController != nil{
//                    for item in 0..<collectionViewManager.cells.count{
//                        if collectionViewManager.cells[item].type == CellTypes.FollowersProfileScreenCell && self.shouldRemoveFollowersProfileScreen == true{
//                            collectionViewManager.cells.remove(at: item)
//                            UIView.performWithoutAnimation {
//                                MainCollectionView.deleteItems(at: [IndexPath(item: item, section: 0)])
//                                MainCollectionView.scrollToItem(at: IndexPath(item: item, section: 0), at: .left, animated: false)
//                            }
//                            return
//                        }
//                        else if collectionViewManager.cells[item].type == CellTypes.MainFeedCollectionViewCell{
//                            collectionViewManager.cells.remove(at: item)
//                            UIView.performWithoutAnimation {
//                            MainCollectionView.deleteItems(at: [IndexPath(item: item, section: 0)])
//                            MainCollectionView.scrollToItem(at: IndexPath(item: item, section: 0), at: .left, animated: false)
//                            }
//                                return
//                            //MainCollectionView.scrollToItem(at: IndexPath(item: item, section: 0), at: .left, animated: false)
//                        }
//                    }
//                }
//                else if sender as? segmentedViewController != nil{
//                   
//                        print("number of cells above us: \(collectionViewManager.cells.count - 1 - currentPage )")
//                    
//                    print("Cell type to be removed: \(cellType)")
//                    print("cell to be removed: \(collectionViewManager.cells.count - 1)")
//                    self.MainCollectionView.performBatchUpdates({() in
//                        print("Visible cells: \(MainCollectionView.visibleCells)")
//                        self.collectionViewManager.cells.remove(at:  collectionViewManager.cells.count - 1)
//                        self.MainCollectionView.deleteItems(at: [IndexPath(item: collectionViewManager.cells.count, section: 0)])
//                        }, completion: {(completed) in
//                            print("Completed batch update: \(completed)")
//                            print("\n")
//                            for cell in self.collectionViewManager.cells{
//                                print("Cells after delete: \(cell.type)")
//                            }
//                            print("\n")
//                            self.removeExcessCells()
//                    })
//                    
////                    self.collectionViewManager.cells.remove(at:  collectionViewManager.cells.count - 1)
////   //                 self.MainCollectionView.deleteItems(at: [IndexPath(item: collectionViewManager.cells.count, section: 0)])\                    //self.collectionViewManager.cells.popLast()
//                    self.profileScreenSegmentedViewController.collectionView.isUserInteractionEnabled = true
//                    //self.MainCollectionView.deleteItems(at: [IndexPath(item: collectionViewManager.cells.count - 1, section: 0)])
//                    //self.MainCollectionView.scrollToItem(at: IndexPath(item: collectionViewManager.cells.count - 1, section: 0), at: .left, animated: true)
//                    self.removeCellCount += 1
//                    print("Remove cell count:\(self.removeCellCount)")
//                    print("visible cells: \(MainCollectionView.indexPathsForVisibleItems)")
//                    print("Cells currently existing: \(MainCollectionView.numberOfItems(inSection: 0))")
//                    
//                    
//                  //  self.MainCollectionView.reloadData()
////                    self.MainCollectionView.scrollToItem(at: IndexPath(item: collectionViewManager.cells.count - 1, section: 0), at: .left, animated: true)
//                }

    }
    
    func removeExcessCells(){
        print("Remove excess cells called")
        for cell in self.collectionViewManager.cells{
            print("cell in array: \(cell.type)")
        }
        if self.collectionViewManager.cells.count > 3 && self.currentPage < self.collectionViewManager.cells.count - 1{
            print("There are excess cells in existance")
            UIView.performWithoutAnimation {
                self.MainCollectionView.performBatchUpdates({
                    var indicies:[IndexPath] = [IndexPath]()
                    for i in 2..<self.collectionViewManager.cells.count - 1{
                        print("number of excessCells: \(i + 1)")
                        self.collectionViewManager.cells.popLast()
                        indicies.append(IndexPath(item:i,section:0))
                    }
                    self.MainCollectionView.deleteItems(at: indicies)
                }, completion: {(completed) in
                    print("Completed deletion of excess cells: \(completed)")
                })
            }

            
        }
    }
    
    func removeAllExcessCells(at index:Int = 0/*,until endIndex:Int? = nil*/,scrollDirection:ScrollDirection,currentPage:Int){
        print("Remove all excess cells called")
        for cell in collectionViewManager.cells{
            print("cell in removeAllExcessCells: \(cell.type)")
        }
        
//        var endingIndex:Int = self.collectionViewManager.cells.count
//
//        if let endIndex = endIndex{
//            endingIndex = endIndex
//        }
        
        print("starting index: \(index)")
        for i in ((index+1)..<collectionViewManager.cells.count).reversed(){
            print("cell type in for loop: \(collectionViewManager.cells[i].type)")
            if(self.collectionViewManager.cells[i].type == .FollowersProfileScreenCell || self.collectionViewManager.cells[i].type == .MainFeedCollectionViewCell){
                self.collectionViewManager.cells.remove(at: i)
                let indexPath = IndexPath(item:i, section: 0)
                if self.selectedUsersDict[indexPath] != nil{
                    self.selectedUsersDict.removeValue(forKey: indexPath)
                }
            }
        }
        self.MainCollectionView.reloadData()
//        var indicies:[IndexPath] = [IndexPath]()
//        UIView.performWithoutAnimation {
//            self.MainCollectionView.performBatchUpdates({
//                [weak self] in
//                guard let self = self else{
//                    return
//                }
//                for i in (index + 1)..<collectionViewManager.cells.count{
//                    if(collectionViewManager.cells[i].type == .FollowersProfileScreenCell || collectionViewManager.cells[i].type == .MainFeedCollectionViewCell){
//                        indicies.append(IndexPath(item: i, section: 0))
//                    }
//                }
//
//                print("Indicies to delete: \(indicies)")
//
//                for i in ((index+1)..<collectionViewManager.cells.count).reversed(){
//                    print("cell type in for loop: \(collectionViewManager.cells[i].type)")
//                    if(self.collectionViewManager.cells[i].type == .FollowersProfileScreenCell || self.collectionViewManager.cells[i].type == .MainFeedCollectionViewCell){
//                        self.collectionViewManager.cells.remove(at: i)
//                        let indexPath = IndexPath(item:i, section: 0)
//                        if self.selectedUsersDict[indexPath] != nil{
//                            self.selectedUsersDict.removeValue(forKey: indexPath)
//                        }
//                    }
//                }
//
//                print("new collectionViewManagerCells: ")
//                for cell in collectionViewManager.cells{
//                    print("Cell: \(cell.type)")
//                }
//
//                self.MainCollectionView.deleteItems(at: indicies)
//
////                var counter:Int = 0
////                for i in indicies{
////                    if i.item <= currentPage{
////                        counter += 1
////                    }
////                }
//
//                let counter = indicies.reduce(0, { (result, indexPath) -> Int in
//                    if(indexPath.item <= currentPage){
//                       return result + 1
//                    }
//                    return result
//                })
//
//                print("scrollDirection: \(scrollDirection), currentPage: \(currentPage), counter: \(counter)")
//                let indexPath = IndexPath(item: currentPage - counter, section: 0)
//                self.MainCollectionView.scrollToItem(at: indexPath, at: .left, animated: false)
////                if scrollDirection == .right{
////                    print("scroll direction right index: \(currentPage - counter)")
////                    let shouldScroll = indicies.reduce(true, {(result,indexPath) in
////                        return result && (indexPath.item <= index)
////                       // self.MainCollectionView.scrollToItem(at: IndexPath(item:currentPage,section:0), at: .right, animated: true)
////                    })
////                    print("should scroll value: \(shouldScroll)")
////                    //self.MainCollectionView.scrollToItem(at: IndexPath(item: index + 1 , section: 0), at: .left, animated: false)
////                    self.MainCollectionView.scrollToItem(at: IndexPath(item:currentPage-counter,section:0), at: .right, animated: true)
////                }else{
////                    print("scroll direction left index: \(index)")
////                self.MainCollectionView.scrollToItem(at: IndexPath(item: currentPage - counter, section: 0), at: .left, animated: false)
////                }
//                    //self.MainCollectionView.scrollToItem(at: currentPage, at: .left, animated: true)
//            }, completion: {(completed) in
//                print("Sucessfully performed batch updates: \(completed)")
//            })
//
//        }
        
    }
    
}


//        self.MainCollectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: false)


