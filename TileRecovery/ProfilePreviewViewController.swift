//
//  ProfilePreviewViewController.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by 123456 on 8/20/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import Foundation
import  UIKit

class ProfilePreviewViewController:UIViewController{
    
    @IBOutlet weak var pictureImageView:UIImageView!
    @IBOutlet weak var topView:UIView!
    @IBOutlet weak var topViewHorrizontalStackView:UIStackView!
    @IBOutlet weak var cancelButton:UIButton!
    @IBOutlet weak var saveButton:UIButton!
    @IBOutlet weak var pictureContainterView: UIView!
    
    var isShouldSave:Bool = true
    
    var imagePass:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topView.backgroundColor = colors.toolBarColor
        topViewHorrizontalStackView.backgroundColor = colors.toolBarColor
        cancelButton.backgroundColor = colors.toolBarColor
        saveButton.backgroundColor = colors.toolBarColor
        pictureImageView.backgroundColor = .green
        
        if let imagePass = imagePass{
            print("image pass is not nil")
            pictureImageView.image = imagePass
            let photoAspectRatio = CGFloat((imagePass.size.width))/CGFloat((imagePass.size.height))
            let cellAspectRatio = CGFloat((self.pictureContainterView.bounds.width/self.pictureContainterView.bounds.height))
            if photoAspectRatio >= cellAspectRatio{
                pictureImageView.frame = CGRect(x: 0, y: 0, width: photoAspectRatio*self.pictureContainterView.bounds.height, height: self.pictureContainterView.bounds.height)
            }
            else{
                pictureImageView.frame = CGRect(x: 0, y: 0, width: self.pictureContainterView.bounds.width, height: self.pictureContainterView.bounds.width/photoAspectRatio)
            }
            //postCell.postImageView.clipsToBounds = true
            self.pictureImageView.clipsToBounds = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("preparing to segue from profilepreviewcontroller")
        if isShouldSave{
            let MVC = segue.destination as! MainViewController
            MVC.setProfileImage(image: self.imagePass!)
            UIImageWriteToSavedPhotosAlbum(self.imagePass!, nil, nil, nil)
        }
        
    }
    
    
    
    
    @IBAction func cancelButton_Tapped(_ sender:UIButton){
        print("cancel button tapped")
        isShouldSave = false
    }
    
    @IBAction func saveButton_Tapped(_ sender:UIButton){
        print("save button tapped 2")
        isShouldSave = true
    }
    
    
}
