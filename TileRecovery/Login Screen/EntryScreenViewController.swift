//
//  EntryScreenViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/9/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit

class EntryScreenViewController:UIViewController{
    
    var tileLabel:UILabel = UILabel()
    var signUpButton:UIButton = UIButton()
    var signInButton:UIButton = UIButton()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = backgroundColor
        self.view.addSubview(tileLabel)
        self.view.addSubview(signUpButton)
        self.view.addSubview(signInButton)
        
        tileLabel.text = "Tile"
        tileLabel.font = UIFont.boldSystemFont(ofSize: 100)
        tileLabel.textColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0)
        tileLabel.textAlignment = .center
        
        signUpButton.setTitle("Create a New Account", for: .normal)
        signUpButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        signUpButton.setTitleColor(UIColor.white, for: .normal)
        signUpButton.backgroundColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0)
        
        signInButton.setTitle("Login", for: .normal)
        signInButton.setTitleColor(UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0), for: .normal)
        signInButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        
        signUpButton.addTarget(self, action: #selector(createNewAccountTapped), for: .touchUpInside)
        signInButton.addTarget(self, action: #selector(loginToAccountTapped), for: .touchUpInside)
        
        setUpLayout()
    }
    
    func setUpLayout(){
        tileLabel.translatesAutoresizingMaskIntoConstraints = false
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        
        signInButton.topAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -15).isActive = true
        signInButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        signInButton.heightAnchor.constraint(equalToConstant: 55).isActive = true
        signInButton.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1.0).isActive = true
        
        signUpButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        signUpButton.heightAnchor.constraint(equalToConstant: 70).isActive = true
        signUpButton.widthAnchor.constraint(equalToConstant: 295).isActive = true
        signUpButton.bottomAnchor.constraint(equalTo: signInButton.topAnchor, constant: -20).isActive = true
        
        tileLabel.heightAnchor.constraint(equalToConstant: 75).isActive = true
        tileLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        tileLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        tileLabel.bottomAnchor.constraint(equalTo: self.signUpButton.topAnchor, constant: -50).isActive = true
    }
    
    @objc func createNewAccountTapped(){
        let signUpVC = SignUpScreenViewController()
//        self.present(signUpVC, animated: false, completion: {
//            print("pushed new viewController")
//        })
 
        //self.navigationController?.pushViewController(signUpVC, animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        present(signUpVC, animated: false, completion: nil)
    }
    
    @objc func loginToAccountTapped(){
        let loginVC = LogInViewController()
        //        self.present(signUpVC, animated: false, completion: {
        //            print("pushed new viewController")
        //        })
        
        //self.navigationController?.pushViewController(signUpVC, animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
        present(loginVC, animated: false, completion: nil)
    }
    
}
