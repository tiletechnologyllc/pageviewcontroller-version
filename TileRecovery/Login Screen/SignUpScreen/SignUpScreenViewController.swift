//
//  ViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/8/18.
//  Copyright © 2018 123456. All rights reserved.
//
//Test that git
import UIKit
import Firebase

var tileColor:UIColor = UIColor(red: 56/255, green: 217/255, blue: 169/255, alpha: 1.0)
var backgroundColor:UIColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)

class SignUpScreenViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UIGestureRecognizerDelegate {
    
    lazy var functions = Functions.functions()
    var db = Database.database()//Firestore.firestore()
    
    
    var scollContainerView:UIScrollView = UIScrollView()
    
    var monthsTableView: UITableView!
    var dayTableView: UITableView!
    var yearTableView: UITableView!
    
    var signUpLabel:UILabel = UILabel()
    var userNameTextField:UITextField = UITextField()
    var firstNameTextField:UITextField = UITextField()
    var lastNameTextField:UITextField = UITextField()
    var emailTextField:UITextField = UITextField()
    var phoneNumberTextField:UITextField = UITextField()
    var passwordTextField:UITextField = UITextField()
    var birthDayLabel:UILabel = UILabel()
    var termsAndPrivacy:UITextView = UITextView()
    var signUpButton:UIButton = UIButton()
    var backButton:UIButton = UIButton()
    
    var incorrectPasswordLabel:UILabel = UILabel()
    var incorrectLabel:UILabel = UILabel()
    
    var incorrectLabelTopAnchor:NSLayoutConstraint!
    var incorrectLabelLeadingAnchor:NSLayoutConstraint!
    var incorrectLabelTrailingAnchor:NSLayoutConstraint!
    var incorrectLabelHeightAnchor:NSLayoutConstraint!
    //var birthdayDatePicker:UIDatePicker = UIDatePicker()
    
    var activeField:UITextField? = nil
    
    
    var showMonthsButton: UIButton!
    var showDaysButton: UIButton!
    var showYearsButton: UIButton!
    
    var newUser = userInfo()
    
    //var month:[String]// = ["January","February","March","April","May","June","July","August","September","November","December"]

//Adjust later
    var monthSelected = 8
    var yearSelected = 1996
    var dateSelected = 7
    
    var daysInMonth = 31
    
    var countryCodeButton:UIButton = UIButton()
    
    var isValidUsername:Bool = true
    
    var allFieldsAccountedFor:Bool = false
    var backSpacePressed:Bool = false
    
    var popUpView:UIView!
    var cancelButton:UIButton!
    var popUpLabel:UILabel!
    var blurEffectView:UIVisualEffectView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterFromKeyboardNotifications()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
         scollContainerView.isScrollEnabled = true
        
        self.view.addSubview(self.scollContainerView)
        self.scollContainerView.backgroundColor = backgroundColor
        self.scollContainerView.contentSize = self.view.frame.size
        
        self.view.backgroundColor = backgroundColor
        
        monthsTableView = UITableView()
        dayTableView = UITableView()
        yearTableView = UITableView()
        
        showMonthsButton = UIButton()
        showDaysButton = UIButton()
        showYearsButton = UIButton()
        
        showMonthsButton.addTarget(self, action: #selector(pickDateSelected(_:)), for: .touchUpInside)
        showDaysButton.addTarget(self, action: #selector(pickDateSelected(_:)), for: .touchUpInside)
        showYearsButton.addTarget(self, action: #selector(pickDateSelected(_:)), for: .touchUpInside)
        
        showYearsButton.setTitle("Year", for: .normal)
        showDaysButton.setTitle("Day", for: .normal)
        showMonthsButton.setTitle("Month", for: .normal)
        
        showMonthsButton.setImage(UIImage(named: "expandArrow"), for: .normal)
        showYearsButton.setImage(UIImage(named: "expandArrow"), for: .normal)
        showDaysButton.setImage(UIImage(named: "expandArrow"), for: .normal)
        
        showYearsButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        showDaysButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        showMonthsButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        
        showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
        showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
        showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
        
        showMonthsButton.setTitleColor(tileColor, for: .focused)
        showYearsButton.setTitleColor(tileColor, for: .focused)
        showDaysButton.setTitleColor(tileColor, for: .focused)
        
        self.scollContainerView.addSubview(monthsTableView)
        self.scollContainerView.addSubview(dayTableView)
        self.scollContainerView.addSubview(yearTableView)
        
        self.scollContainerView.addSubview(showMonthsButton)
        self.scollContainerView.addSubview(showDaysButton)
        self.scollContainerView.addSubview(showYearsButton)
        
        monthsTableView.delegate = self
        monthsTableView.dataSource = self
        monthsTableView.layer.cornerRadius = 5
       // monthsTableView.layer.borderWidth = 1
       // monthsTableView.layer.borderColor = UIColor.black.cgColor
        monthsTableView.separatorStyle = .none
        monthsTableView.backgroundColor = backgroundColor
        monthsTableView.isHidden = true
        
        yearTableView.delegate = self
        yearTableView.dataSource = self
        yearTableView.layer.cornerRadius = 5
        yearTableView.backgroundColor = backgroundColor
       // yearTableView.layer.borderWidth = 1
       // yearTableView.layer.borderColor = UIColor.black.cgColor
        yearTableView.separatorStyle = .none
        yearTableView.isHidden = true
        
        dayTableView.delegate = self
        dayTableView.dataSource = self
        dayTableView.layer.cornerRadius = 5
//        dayTableView.layer.borderWidth = 1
//        dayTableView.layer.borderColor = UIColor.black.cgColor
        dayTableView.backgroundColor = backgroundColor
        dayTableView.separatorStyle = .none
        dayTableView.isHidden = true
        
        
        showMonthsButton.layer.cornerRadius = 5
//        showMonthsButton.layer.borderColor = tileColor.cgColor
//        showMonthsButton.layer.borderWidth = 1
        
        showDaysButton.layer.cornerRadius = 5
//        showDaysButton.layer.borderColor = tileColor.cgColor
//        showDaysButton.layer.borderWidth = 1
        
        showYearsButton.layer.cornerRadius = 5
//        showYearsButton.layer.borderColor =  tileColor.cgColor
//        showYearsButton.layer.borderWidth = 1
        
        let month = Calendar.current.monthSymbols
        for mon in month{
            print(mon)
        }
        
        self.scollContainerView.addSubview(signUpLabel)
        self.scollContainerView.addSubview(userNameTextField)
        self.scollContainerView.addSubview(firstNameTextField)
        self.scollContainerView.addSubview(lastNameTextField)
        self.scollContainerView.addSubview(emailTextField)
        self.scollContainerView.addSubview(phoneNumberTextField)
        self.scollContainerView.addSubview(passwordTextField)
        self.scollContainerView.addSubview(birthDayLabel)
        self.scollContainerView.addSubview(termsAndPrivacy)
        self.scollContainerView.addSubview(signUpButton)
        self.scollContainerView.addSubview(backButton)
        
        self.scollContainerView.addSubview(incorrectPasswordLabel)
        self.scollContainerView.addSubview(incorrectLabel)
        
        self.scollContainerView.addSubview(countryCodeButton)
        
        passwordTextField.isSecureTextEntry = true
        passwordTextField.delegate = self
        
        signUpButton.backgroundColor = tileColor
        signUpButton.setTitleColor(backgroundColor, for: .normal)
        signUpButton.setTitle("Sign Up", for: .normal)
        signUpButton.layer.cornerRadius = 5
        
        emailTextField.delegate = self
        userNameTextField.delegate = self
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        //phoneNumberTextField.delegate = self
        
        termsAndPrivacy.isUserInteractionEnabled = true
        
        setUpLayout()
        
        self.scollContainerView.sendSubviewToBack(termsAndPrivacy)
        self.scollContainerView.sendSubviewToBack(signUpButton)
        
        termsAndPrivacy.textContainer.lineBreakMode = .byWordWrapping
        termsAndPrivacy.text = "By signing up, you agree to our Terms of Service & Privacy Policy."
        termsAndPrivacy.textColor = tileColor
        
        termsAndPrivacy.isEditable = false
        
        let attributedString = NSMutableAttributedString(string: "By signing up, you agree to our Terms of Service & Privacy Policy.")
        attributedString.addAttribute(.link, value: "https://tilesocial.com", range: NSRange(location: 31, length: 17))
        attributedString.addAttribute(.link, value: "https://tilesocial.com", range: NSRange(location: 51, length: 14))
        
        //attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: tileColor , range: NSRange(location: 31, length: 17))
       
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: tileColor , range:  NSRange(location: 51, length: 14))
        
        termsAndPrivacy.attributedText = attributedString
        termsAndPrivacy.textColor = tileColor.withAlphaComponent(0.7)
        termsAndPrivacy.tintColor = tileColor
        
        backButton.setImage(#imageLiteral(resourceName: "icons8-back-96"), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)

//        scollContainerView.bringSubviewToFront(signUpButton)
//        scollContainerView.sendSubviewToBack(termsAndPrivacy)
    

        
        let scollViewTapGestureRecogizer = UITapGestureRecognizer(target: self, action: #selector(scrollViewTapped))
        scollViewTapGestureRecogizer.cancelsTouchesInView = false
        scollViewTapGestureRecogizer.delegate = self
        scollContainerView.addGestureRecognizer(scollViewTapGestureRecogizer)

        scollContainerView.isScrollEnabled = true
        //scollContainerView.isMultipleTouchEnabled = true
        
        signUpButton.addTarget(self, action: #selector(signUpButtonTapped), for: .touchUpInside)
        phoneNumberTextField.addTarget(self, action: #selector(textFieldDidCange(textField:)), for: .editingChanged)
        phoneNumberTextField.delegate = self
        phoneNumberTextField.keyboardType = .numberPad
        phoneNumberTextField.textContentType = UITextContentType.telephoneNumber
        //phoneNumberTextField.text = "+1"
        
        countryCodeButton.setTitle("+1", for: .normal)
        countryCodeButton.backgroundColor = backgroundColor
        countryCodeButton.titleLabel?.textColor = tileColor
    
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view == signUpButton{
            return false
        }
        return true
    }
    
    @objc func scrollViewTapped(){
        print("Scroll view tapped")
        view.endEditing(true)
        resignFirstResponder()
    }
    
//    @objc func signUpButtonTapped(){
//        phoneNumberTextField.resignFirstResponder()
//        //Auth.auth().
//        
//        if newUser.isNill == false{
//        
//        guard let phoneNumber =  newUser.phoneNumber else{
//            print("No phone number entered")
//            return
//        }
//        let verifyViewController = VerifyViewController()
//        verifyViewController.phoneNumber = phoneNumber
//        
//        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
//            if let error = error {
//                //self.showMessagePrompt(error.localizedDescription)
//                print("An error has occured while trying to authenticate phone number: \(error.localizedDescription)")
//                return
//            }
//            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
//            // Sign in using the verificationID and the code sent to the user
//            // ...
//            verifyViewController.verificationID = verificationID
//            print("Verification id created")
//            //semd to firestore
//            
//        }
        
//        let transition = CATransition()
//        transition.duration = 0.3
//        transition.type = CATransitionType.push
//        transition.subtype = CATransitionSubtype.fromRight
//        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//        view.window!.layer.add(transition, forKey: kCATransition)
//        
//        self.present(verifyViewController, animated: false, completion: {
//            print("presented Verify View Controller")
//        })
//            
//        }
//        
//        else{
//            print("Not all fields have been filled")
//        }
//    }
    
    
    @objc func backButtonTapped(){
//        self.dismiss(animated: true, completion: {
//            print("Sign Up Screen Dismissed")
//        })
 //       self.navigationController?.popViewController(animated: true)
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        
      dismiss(animated: true, completion: nil)
    }
    


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == monthsTableView{
            return Calendar.current.monthSymbols.count
        }
        else if tableView == yearTableView{
            return 150
        }
        else if tableView == dayTableView{
            let dateComponents = DateComponents(year: yearSelected, month: monthSelected)
            let calendar = Calendar.current
            let date = calendar.date(from: dateComponents)!
            
            let range = calendar.range(of: .day, in: .month, for: date)!
            let numDays = range.count
            daysInMonth = numDays
            return numDays
        }
        
        return 0
        
        }
    
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 1.0)
        
        if tableView == monthsTableView{
            showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
           // showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
           // showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            
        showMonthsButton.setTitle("Month", for: .normal)
            
        let cell = UITableViewCell()
        cell.textLabel?.text = "\(Calendar.current.monthSymbols[indexPath.row])"
        cell.textLabel?.textAlignment = .center
        cell.selectedBackgroundView = backgroundView
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        cell.textLabel?.textColor = tileColor
        cell.textLabel?.backgroundColor = backgroundColor
        cell.backgroundColor = backgroundColor
        return cell
        }
        else if tableView == dayTableView{
            
           // showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
           // showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            
            showDaysButton.setTitle("Day", for: .normal)
            
            let cell = UITableViewCell()
            
            
            cell.textLabel?.text = "\(indexPath.row + 1)"
            cell.textLabel?.textAlignment = .center
            cell.selectedBackgroundView = backgroundView
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            cell.textLabel?.textColor = tileColor
            cell.textLabel?.backgroundColor = backgroundColor
            cell.backgroundColor = backgroundColor
            return cell
            
        }
            
        else if tableView == yearTableView{
            
            //showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            //showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            
            showYearsButton.setTitle("Year", for: .normal)
            
            let cell = UITableViewCell()
            let year = Calendar.current.component(.year, from: Date())
            
            cell.textLabel?.isEnabled = true
            
            if monthSelected == 2 && dateSelected == 29{
                if  (year - 13 - indexPath.row) % 4 != 0 {
                    cell.textLabel?.isEnabled = false
                }
                else{
                    cell.textLabel?.isEnabled = true
                }
            }
            cell.textLabel?.text = "\(year - 13 - indexPath.row)"
            cell.textLabel?.textAlignment = .center
            cell.selectedBackgroundView = backgroundView
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            cell.textLabel?.textColor = tileColor
            cell.textLabel?.backgroundColor = backgroundColor
            cell.backgroundColor = backgroundColor
            return cell
        }
        return UITableViewCell()
    }

    @objc func pickDateSelected(_ sender: UIButton) {
        resignFirstResponder()
        if sender == showMonthsButton{
            showMonthsButton.setTitleColor(tileColor.withAlphaComponent(1.0), for: .normal)
            showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            print("showMonthsButton selected")
            dayTableView.isHidden = true
            yearTableView.isHidden = true
            if monthsTableView.isHidden == true{
                monthsTableView.isHidden = false
            }
            else{
                monthsTableView.isHidden = true
            }
        }
        else if sender == showDaysButton{
            showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showYearsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showDaysButton.setTitleColor(tileColor.withAlphaComponent(1.0), for: .normal)
            monthsTableView.isHidden = true
            yearTableView.isHidden = true
            if dayTableView.isHidden == true{
                dayTableView.isHidden = false
            }
            else{
                dayTableView.isHidden = true
            }
        }
        else if sender == showYearsButton{
            showMonthsButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            showYearsButton.setTitleColor(tileColor.withAlphaComponent(1.0), for: .normal)
            showDaysButton.setTitleColor(tileColor.withAlphaComponent(0.5), for: .normal)
            monthsTableView.isHidden = true
            dayTableView.isHidden = true
            if yearTableView.isHidden == true{
                yearTableView.isHidden = false
            }
            else{
                yearTableView.isHidden = true
            }
        }
    }
    
    
    var isDateSelected:Bool = false, isMonthWasSelected:Bool = false, isYearWasSelected:Bool = false
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        isInvalidEntry(isInvalid: false, superView: birthDayLabel)
        
        if tableView == monthsTableView{
        let selectedMonth = Calendar.current.monthSymbols[indexPath.row]
        self.monthSelected = indexPath.row + 1
        dayTableView.reloadData()
     //   monthsTableView.reloadData()
        yearTableView.reloadData()
        showMonthsButton.setTitle(Calendar.current.monthSymbols[indexPath.row], for: .normal)
        monthsTableView.isHidden = true
            isMonthWasSelected = true
            }
        
        else if tableView == dayTableView{
            self.dateSelected = indexPath.row + 1
        //    dayTableView.reloadData()
        //    monthsTableView.reloadData()
            yearTableView.reloadData()
            showDaysButton.setTitle(String(dateSelected), for: .normal)
            dayTableView.isHidden = true
            isDateSelected = true
        }
        
        else if tableView == yearTableView{
            let year = Calendar.current.component(.year, from: Date())
            self.yearSelected = year - 13 - indexPath.row
            showYearsButton.setTitle(String(yearSelected), for: .normal)
            yearTableView.isHidden = true
            isYearWasSelected = true
        }
        
        if isMonthWasSelected == true && isYearWasSelected == true && isDateSelected == true{
            if let birthday = makeDate(year: yearSelected, month: monthSelected, day: dateSelected){
                newUser.Birthday = birthday
            }
            else{
                print("Error in setting Birthday")
                allFieldsAccountedFor = false
                isInvalidEntry(isInvalid: true, superView: birthDayLabel, message: "Invlaid Birthday Entered")
            }
            print("newUserBirthday: \(newUser.Birthday)")
        }

        

        
//        var monthName:String = ""
//
//        switch selectedMonth {
//        case 1:
//            print("January")
//            monthName = "January"
//        case 2:
//            print("February")
//            monthName = "February"
//        case 3:
//            print("March")
//            monthName = "March"
//        case 4:
//            print("April")
//            monthName = "April"
//        case 5:
//            print("May")
//            monthName = "May"
//        case 6:
//            print("June")
//            monthName = "June"
//        case 7:
//            print("July")
//            monthName = "July"
//        case 8:
//            print("August")
//            monthName = "August"
//        case 9:
//            print("September")
//            monthName = "Septemeber"
//        case 10:
//            print("October")
//            monthName = "October"
//        case 11:
//            print("November")
//            monthName = "November"
//        case 12:
//            print("December")
//            monthName = "December"
//        default:
//            print("Invalid Month")
//            assert(true, "Invalid Month Selected")
//        }
        
    }
    
    func makeDate(year: Int, month: Int, day: Int) -> Date?{
        if isDateValid(month: month, day: day, year: year){
            var calendar = Calendar(identifier: .gregorian)
            // calendar.timeZone = TimeZone(secondsFromGMT: 0)!
            let components = DateComponents(year: year, month: month, day: day)
            return calendar.date(from: components)!
        }
        else{
            allFieldsAccountedFor = false
            //    isInvalidEntry(isInvalid: true, superView: T##UIVie, message: <#T##String#>)
            return nil
        }
    }
    
    
    func isDateValid(month:Int,day:Int,year:Int)->Bool{
        let dateString = String(day)+":"+String(month)+":"+String(year)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd:MM:yyyy"
        if dateFormatter.date(from: dateString) != nil {
            print("date is valid")
            return true
        } else {
            print("date is invalid")
            return false
        }
    }

}



