//
//  keyboard-SignUpViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/22/18.
//  Copyright © 2018 123456. All rights reserved.
//

import Foundation
import UIKit

extension SignUpScreenViewController{
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification , object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification , object: nil)
    }
    
    @objc func keyboardWillShow(notification:Notification){
        guard let keyboardFrame: CGRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        

             if let activeField = self.activeField{
                print("keyboard will show called")
        scollContainerView.contentInset.bottom = keyboardFrame.height + 20
        scollContainerView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)

        print("textField 2 frame: \(activeField.frame)")
         scollContainerView.scrollRectToVisible(activeField.frame, animated: true)
         // scollContainerView.setContentOffset(CGPoint(x: 0, y: keyboardFrame.height - 20), animated: true)
        }
    }
    
    @objc func keyboardWillChange(){
        
    }
    
    @objc func keyboardWillHide(){
        scollContainerView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    
    
//
//    @objc func keyboardWillShow(notification: NSNotification) {
//        print("keyboard will show called")
//        var info = notification.userInfo
//        let keyBoardSize = info![UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
//        scollContainerView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyBoardSize.height, right: 0.0)
//        scollContainerView.scrollIndicatorInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyBoardSize.height, right: 0.0)
//    }
//
//    @objc func keyboardWillHide(notification: NSNotification){
//
//    }
//
}
