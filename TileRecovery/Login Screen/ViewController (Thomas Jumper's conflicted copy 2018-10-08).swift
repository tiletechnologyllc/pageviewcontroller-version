//
//  ViewController.swift
//  TableViewDropDown
//
//  Created by 123456 on 10/8/18.
//  Copyright © 2018 123456. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var showButton: UIButton!
    
    var month:[Int] {
        var arr:[Int] = [Int]()
        for i in 1..<13{
            arr.append(i)
        }
        return arr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.cornerRadius = 5
        tableView.layer.borderWidth = 1
        tableView.layer.borderColor = UIColor.black.cgColor
        tableView.separatorStyle = .none
        
        showButton.layer.cornerRadius = 5
        showButton.layer.borderColor = UIColor.black.cgColor
        showButton.layer.borderWidth = 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return month.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "\(month[indexPath.row])"
        cell.textLabel?.textAlignment = .center
        return cell
    }

    @IBAction func pickDateSelected(_ sender: Any) {
        if tableView.isHidden == true{
            tableView.isHidden = false
        }
        else{
            tableView.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = month[indexPath.row]
        showButton.setTitle(String(selectedItem), for: .normal)
        
        switch selectedItem {
        case 0:
            print("January")
        case 1:
            print("February")
        case 2:
            print("March")
        case 3:
            print("April")
        case 4:
            print("May")
        case 5:
            print("June")
        case 6:
            print("July")
        case 7:
            print("August")
        case 8:
            print("September")
        case 9:
            print("October")
        case 10:
            print("November")
        case 11:
            print("December")
        default:
            print("Invalid Month")
            assert(true, "Invalid Month Selected")
        }
    }
    
    func isDateValid(month:Int,day:Int,year:Int){
        
    }
}

