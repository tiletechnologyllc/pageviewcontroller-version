//
//  FeedViewControllerTableCell.swift
//  collectionViewAttemptOfPagingAttempt002
//
//  Created by Zach on 2/8/18.
//  Copyright © 2018 TheRedCamaro. All rights reserved.
//

import UIKit
import FirebaseFunctions

enum reportDeleteButtonType{
    case deleteButton
    case reportButton
}

protocol deletePhotoFromTableView: class{
    func deletePhotoFromTable(cell:FeedViewControllerTableCell)
}

class FeedViewControllerTableCell: UITableViewCell {
    
    weak var deletePhotoDelegate:deletePhotoFromTableView? = nil

    var postID:String!
    var post:Post!
    var userLikedPost:Bool!
    
    lazy var functions:Functions = Functions.functions()
    
    var containerView: UIView = UIView()
    var cellImageView: UIImageView = UIImageView()
    var cellLabel:UILabel = UILabel()
    var likesLabel: UILabel = UILabel()
    
    var ublurredImage:UIImage!
    var reportLabel:UILabel!
    
    var shareButton:UIButton!
    var reportDeleteButton:UIButton!
    
    var HorrizontalStackView:UIStackView!
    
    var buttonType:reportDeleteButtonType!
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        cellLabel.font = UIFont.boldSystemFont(ofSize: 16)
        cellLabel.textColor = backgroundColor
        likesLabel.font = UIFont.boldSystemFont(ofSize: 16)
        likesLabel.textColor = backgroundColor
        // Initialization code
        self.addSubview(containerView)
        containerView.backgroundColor = colors.backgroundColor
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(userSwipedOnCell(_:)))
        leftSwipe.cancelsTouchesInView = false
        leftSwipe.direction = .left

        self.addGestureRecognizer(leftSwipe)
        
        self.containerView.translatesAutoresizingMaskIntoConstraints = false
        self.containerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.containerView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        self.containerView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        self.containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        
        self.containerView.addSubview(self.cellImageView)
        self.cellImageView.translatesAutoresizingMaskIntoConstraints = false
        self.cellImageView.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 0).isActive = true
        self.cellImageView.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 0).isActive = true
        self.cellImageView.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: 0).isActive = true
        self.cellImageView.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: -10).isActive = true
        
        self.cellImageView.addSubview(self.cellLabel)
        self.cellLabel.translatesAutoresizingMaskIntoConstraints = false
        self.cellLabel.leadingAnchor.constraint(equalTo: self.cellImageView.leadingAnchor, constant: 0).isActive = true
        self.cellLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        self.cellLabel.topAnchor.constraint(equalTo: self.cellImageView.topAnchor, constant: 0).isActive = true
        self.cellLabel.trailingAnchor.constraint(equalTo: self.cellImageView.trailingAnchor, constant: -26).isActive = true
        
        self.cellImageView.addSubview(self.likesLabel)
        self.likesLabel.translatesAutoresizingMaskIntoConstraints = false
        self.likesLabel.trailingAnchor.constraint(equalTo: self.cellImageView.trailingAnchor, constant: 0).isActive = true
        self.likesLabel.leadingAnchor.constraint(equalTo: self.cellImageView.leadingAnchor, constant: 0).isActive = true
        self.likesLabel.bottomAnchor.constraint(equalTo: self.cellImageView.bottomAnchor, constant: -10).isActive = true
        self.likesLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        self.cellImageView.isUserInteractionEnabled = true
        let doubleTapGesture:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        doubleTapGesture.numberOfTapsRequired = 2
        self.addGestureRecognizer(doubleTapGesture)
        //setUpButtons()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func tap(){
        print("tap button tapped")
    }
    
    var isBlur:Bool = false
    
    @objc func userSwipedOnCell(_ sender:UISwipeGestureRecognizer){
        print("userSwipedCell")
        if sender.direction == .left{
            print("User swiped cell left")
            if isBlur == false{
                isBlur = !isBlur
                self.cellImageView.image = blurEffect(image: self.cellImageView.image!)
                //setUpButtons()
                reportDeleteButton.isHidden = false
                shareButton.isHidden = false
                isBlur = true
            }
        }
    }
    
    var context = CIContext(options: nil)
    
    func blurEffect(image:UIImage)->UIImage {
        
        let currentFilter = CIFilter(name: "CIGaussianBlur")
        let beginImage = CIImage(image: image)
        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
        currentFilter!.setValue(10, forKey: kCIInputRadiusKey)
        
        let cropFilter = CIFilter(name: "CICrop")
        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
        
        let output = cropFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        //   cellImageView.image = processedImage
        return processedImage
    }
    
    func setUpButtons(){
        shareButton = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 30))
        reportDeleteButton = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 30))
        
        shareButton.translatesAutoresizingMaskIntoConstraints = false
        reportDeleteButton.translatesAutoresizingMaskIntoConstraints = false
        
        shareButton.setTitle("Share", for: .normal)
        shareButton.setImage(#imageLiteral(resourceName: "icons8-share-500"), for: .normal)
        shareButton.titleEdgeInsets = UIEdgeInsets.init(top: 150, left: -500 ,bottom: 30, right: 0)
        shareButton.imageEdgeInsets = UIEdgeInsets.init(top: 5, left: 5 ,bottom: 35, right: 5)
        shareButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 25)
        shareButton.setTitleColor(UIColor.white, for: .normal)
        
        print("In set up buttons")
        
        if self.buttonType == reportDeleteButtonType.deleteButton{
            print("Of type delete button")
            reportDeleteButton.setTitle("Delete", for: .normal)
            reportDeleteButton.setImage(#imageLiteral(resourceName: "icons8-close-window-500"), for: .normal)
            reportDeleteButton.addTarget(self, action: #selector(deleteButton_Tapped), for: .touchUpInside)
            reportDeleteButton.titleEdgeInsets = UIEdgeInsets.init(top: 150, left: -500 ,bottom: 30, right: 0)
        }
        else if self.buttonType == reportDeleteButtonType.reportButton{
            reportDeleteButton.setTitle("Report", for: .normal)
            reportDeleteButton.setImage(#imageLiteral(resourceName: "icons8-system-report-96"), for: .normal)
            reportDeleteButton.addTarget(self, action: #selector(reportButton_Tapped), for: .touchUpInside)
            reportDeleteButton.titleEdgeInsets = UIEdgeInsets.init(top: 150, left: -100 ,bottom: 30, right: 0)
        }
        else{
            print("No button type called: \(self.buttonType)")
        }
        
        //reportDeleteButton.titleEdgeInsets = UIEdgeInsetsMake(150, -500 ,30, 0)
        reportDeleteButton.imageEdgeInsets = UIEdgeInsets.init(top: 5, left: 20 ,bottom: 45, right: 20)
        reportDeleteButton.titleLabel!.font = UIFont.boldSystemFont(ofSize: 25)
        reportDeleteButton.setTitleColor(UIColor.white, for: .normal)
        
        
        shareButton.backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 0.3)
        reportDeleteButton.backgroundColor = UIColor(red: 230/255, green: 252/255, blue: 245/255, alpha: 0.3)
        
        
        shareButton.addTarget(self, action: #selector(shareButton_Tapped), for: .touchUpInside)
        
        
        // HorrizontalStackView = UIStackView(arrangedSubviews: [shareButton,reportButton,deleteButton])
        
        
        HorrizontalStackView = UIStackView(arrangedSubviews: [shareButton,reportDeleteButton])
        HorrizontalStackView.axis = .horizontal
        HorrizontalStackView.distribution = .equalSpacing
        // HorrizontalStackView.spacing = 25
        self.addSubview(HorrizontalStackView)
        
        self.addSubview(HorrizontalStackView)
        
        HorrizontalStackView.translatesAutoresizingMaskIntoConstraints = false
        HorrizontalStackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20).isActive = true
        HorrizontalStackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive = true
        HorrizontalStackView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        HorrizontalStackView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        HorrizontalStackView.alignment = .center
        
        shareButton.heightAnchor.constraint(equalTo: HorrizontalStackView.heightAnchor).isActive = true
        reportDeleteButton.heightAnchor.constraint(equalTo: HorrizontalStackView.heightAnchor, constant: 0).isActive = true
        
        shareButton.widthAnchor.constraint(equalTo: HorrizontalStackView.heightAnchor).isActive = true
        reportDeleteButton.widthAnchor.constraint(equalTo: HorrizontalStackView.heightAnchor).isActive = true
        
        shareButton.layer.cornerRadius = 150/5
        shareButton.layer.masksToBounds = true
        
        reportDeleteButton.layer.cornerRadius = 150/5
        reportDeleteButton.layer.masksToBounds = true
        
        shareButton.isHidden = true
        reportDeleteButton.isHidden = true
        reportLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        self.addSubview(reportLabel)
        reportLabel.translatesAutoresizingMaskIntoConstraints = false
        reportLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        reportLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        reportLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        reportLabel.heightAnchor.constraint(equalToConstant: 200).isActive = true
        reportLabel.text = "Your report has been reported to to the highest of reporters"
        reportLabel.numberOfLines = 0
        reportLabel.textColor = colors.backgroundColor
        reportLabel.font = UIFont.boldSystemFont(ofSize: 20)
        reportLabel.isHidden = true
        reportLabel.textAlignment = .center
    }
    
    @objc func shareButton_Tapped(){
        print("Share Button Tapped")
        shareButton.isHidden = true
        reportDeleteButton.isHidden = true
        self.cellImageView.image = self.ublurredImage
    }
    
    @objc func reportButton_Tapped(){
        print("Report Button Tapped")
        shareButton.isHidden = true
        reportDeleteButton.isHidden = true
        reportLabel.isHidden = false
        //Add in something for the server here
    }
    
    @objc func deleteButton_Tapped(){
        print("Delete Button Tapped")
        shareButton.isHidden = true
        reportDeleteButton.isHidden = true
        deletePhotoDelegate?.deletePhotoFromTable(cell:self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension FeedViewController:deletePhotoFromTableView{
    func deletePhotoFromTable(cell:FeedViewControllerTableCell) {
        print("Delete photo from table reached")
        let indexPath = tableView.indexPath(for: cell)!
        self.images.remove(at: indexPath.item)
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
        //Add in delete from server
    }
}
