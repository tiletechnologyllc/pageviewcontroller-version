//
//  ProfilePostsSelectedDelegate.swift
//  TileRecovery
//
//  Created by 123456 on 3/25/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit


protocol ProfilePostsSelected: class{
    func profilePostPass(indexPath:IndexPath, segmentType:segmentType?,name:String,id:String)
}


