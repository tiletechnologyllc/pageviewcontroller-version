//
//  Database.swift
//  TileRecovery
//
//  Created by 123456 on 3/25/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension MainProfileScreenCell{
    func getFollowers(){
        var followerIDs:[String] = [String]()
        let userID = self.userID//"dVC63JFr8YWHGkiSsQFDsW9SmOF2"
        print("Followers userID: \(userID)")
        Database.database().reference(withPath: "Followers/\(userID)").observeSingleEvent(of:.value, with: {[weak self] (snapshot) in
            guard let self = self else{
                assert(true,"self found nil")
                print("self found nil")
                return
            }
            if let data = snapshot.value as? [String:Any]{
                //print("Data: \(data)")
                followerIDs = Array(data.keys)
                self.getFollowerData(followers: followerIDs)
            }else{
                print("Failed to get follwer data")
            }
            }, withCancel: {(error) in
                print("Error getting follwers data: \(error)")
        })
        
    }
    
    func getFollowing(){
        print("Get following called for userID: \(self.userID)")
        //self.following = []
        //self.collectionView.reloadData()
        var followingIDs:[TagUser] = [TagUser]()
        let userID = self.userID//"dVC63JFr8YWHGkiSsQFDsW9SmOF2"
        
        self.followingDispatchGroup.enter()
        Database.database().reference(withPath: "Following/\(userID)/tagsUsers").queryOrdered(byChild: "negativeLikesCount").observeSingleEvent(of:.value, with: {[weak self] (snapshot) in
            // print("snapshot returned from query: \(snapshot.value)")
            guard let self = self else{
                assert(true,"self found nil")
                print("self found nil")
                return
            }
            
            
            for child in snapshot.children{
                //print("child in snapshot: \(child)")
                if let snap = child as? DataSnapshot{
                    guard let data = snap.value as? [String:Any] else{
                        print("Error converting snap to [String:Any]")
                        return
                    }
                    //print("Data: \(data)")
                    
                    print("Following IDs: \(snap.key)")
                    
                    if let isUser = data["isUser"] as? Bool{
                        if isUser{
                            followingIDs.append(TagUser(ID: snap.key, type: .user))
                        }
                        else{
                            followingIDs.append(TagUser(ID: snap.key, type: .post))
                        }
                    }else{
                        print("Failed to parse data")
                    }
                    
                }else{
                    print("Failed to convert data to string any")
                }
            }
            
            print("followIDs as they appear from database: ")
            for follow in followingIDs{
                print("followIDS: \(follow.ID)")
            }
            
            self.followingDispatchGroup.leave()
            
            let blockedUserDispatchGroup = DispatchGroup()
            for i in 0..<followingIDs.count{
                
                blockedUserDispatchGroup.enter()
                if followingIDs[i].type == .user{
                    self.isUserBlocked(userID: followingIDs[i].ID,index: i, completion: {(isBlocked,index) in
                        if isBlocked{
                            followingIDs.remove(at: index)
                        }else{
                            return
                        }
                    })
                }
                blockedUserDispatchGroup.leave()
            }
            
            blockedUserDispatchGroup.notify(queue: .main, execute: { [weak self] in
                guard let self = self else{
                    assert(true,"self found nil")
                    print("self found nil")
                    return
                }
                self.getFollowingData(followings: followingIDs)
                followingIDs.removeAll()
            })
            
            
            
            
            }, withCancel: {(error) in
                print("An error occured in getting the value: \(error)")
        })
        
        //  self.getFollowingData(followings: followingIDs)
        
        //        self.followingDispatchGroup.notify(queue: .main, execute: {()
        //            print("Dispatch queeue in getFollowing: \(self.following.count)")
        //            print("following after function: ")
        //            for follow in self.following{
        //                if let follow = follow as? User{
        //                print(follow.userID)
        //                }else if let follow = follow as? Tag{
        //                    print(follow.tagID)
        //                }
        //            }
        //            self.collectionView.reloadData()
        //        })
        //        self.followingDispatchGroup.notify(queue: .main, execute: {() in
        //            print("mission success dispatch group complete")
        //            self.collectionView.reloadData()
        //        })
        
        //        self.collectionView.reloadData()
        //self.getFollowingData(followings: followingIDs)
        
    }
    
    func isUserBlocked(userID:String,index:Int, completion: @escaping ((Bool,Int) -> Void)){
        Database.database().reference(withPath: "BlockedUsers/\(User.signedInUserID)/\(userID)").observe(.value, with: {[weak self] (dataSnapshot) in
            
            guard let self = self else{
                assert(true,"self found nil")
                print("self found nil")
                return
            }
            
            if dataSnapshot.exists(){
                completion(true,index)
            }else{
                completion(false,index)
            }
            }, withCancel: {(error) in
                print("An error occured getting the blockedUsers: \(error)")
                completion(false,index)
        })
        
    }
    
    
    func getUserPosts(){
        self.userPosts = []
        let userID = self.userID//"PXzzVSFiqxeXngRFfr0qwN53TKZ2"
        Database.database().reference(withPath: "PostIDS").queryOrdered(byChild: "userID").queryEqual(toValue: userID).observeSingleEvent(of: .value, with: {[weak self] (snapshot) in
            guard let self = self else{
                assert(true,"self found nil")
                print("self found nil")
                return
            }
            //            if let data = snapshot.value as? [DataSnapshot]{
            //                print("Data: \(data)")
            //            }else{
            //                print("Failed to get Post data")
            //            }
            
            for child in snapshot.children{
                print("Child: \(child)")
                if let childSnap = child as? DataSnapshot, let data = childSnap.value as? [String:Any]{
                    // print("Data: \(data)")
                    
                    
                    guard let storageRef = data["storageRef"] as? String, let likes = data["likes"] as? [String:Any], let totalLikes = likes["totalLikes"] as? Int, let userID = data["userID"] as? String, let tagID = data["tagID"] as? String, let photoURL = data["photoURL"] as? String else{
                        print("There was an error parsing the post data")
                        continue
                    }
                    
                    print("child snap.key: \(childSnap.key)")
                    let post = Post(storageRef: storageRef, totalLikes: totalLikes, postID: childSnap.key, userID: userID,tagID:childSnap.key,isUser:true,photoURL:photoURL)
                    self.userPosts.append(post)
                    Post.setPostToCache(post: post, tagUserID: userID)
                }else{
                    print("Failed to get posts")
                }
                
                //                guard let storageRef = child[]
                //                let Post = Post(storageRef: <#T##String#>, totalLikes: <#T##Int#>, postID: <#T##String#>, userID: <#T##String#>)
                //self.collectionView.reloadData()
                self.getTagNames(posts: self.userPosts)
            }
            
            }, withCancel: {(error) in
                print("An error occured while getting posts: \(error)")
        })
    }
    
    func getTagNames(posts:[Post]){
        let userID = self.userID//"PXzzVSFiqxeXngRFfr0qwN53TKZ2"
        for post in posts{
            print("TagID: \(post.tagID)")
            Database.database().reference(withPath: "TagIDs/\(post.tagID!)").observeSingleEvent(of: .value, with: {[weak self] (snapshot) in
                guard let self = self else{
                    assert(true,"self found nil")
                    print("self found nil")
                    return
                }
                // print("Snapshot: \(snapshot.value)")
                if let data = snapshot.value as? [String:Any]{
                    post.tagName = data["name"] as? String
                    print("Tag name: \(post.tagName)")
                    Post.setPostToCache(post: post, tagUserID: userID)
                }else{
                    print("Error getting name")
                }
                self.collectionView.reloadData()
                }, withCancel: {(error) in
                    print("An error occured: \(error)")
            })
        }
        
    }
    
    func getFollowingData(followings:[TagUser]){
        //possibly delete
        self.getFollowingCount += 1
        print("Get following count: \(self.getFollowingCount)")
        // self.following = []
        //self.collectionView.reloadData()
        print("Follwing beofre: \(self.following)")
        
        
        for follower in self.following{
            if let follower = follower as? User{
                print("Following array before: \(follower.userID)")
            }else if let follower = follower as? Tag{
                print("Following array before: \(follower.tagID)")
            }else{
                print("failed to parse before array")
            }
        }
        
        getTheDatas(followings: followings)
    }
    
    
    func getFollowerData(followers:[String]){
        //possibly delete
        self.followers = []
        
        for follower in followers{
            if let user = User.followersCache.object(forKey: follower as NSString){
                //look into removing append
                self.followers.append(user)
            }
            else{
                print("Searching database for follower: \(follower)")
                Database.database().reference(withPath: "Users").queryOrderedByKey().queryEqual(toValue: follower).observeSingleEvent(of: .value, with: {[weak self] (snapshot) in
                    guard let self = self else{
                        assert(true,"self found nil")
                        print("self found nil")
                        return
                    }
                    //print("Friends data: \(snapshot.value)")
                    let data = snapshot.value as!  [String:Any]
                    for(key,value) in data{
                        //print("Key: \(key) => value: \(value)")
                        let element = value as! [String:Any]
                        guard let username = element["username"] as? String, let photoURL = element["photoURL"] as? String else{
                            print("Could not parse all of the values")
                            break
                        }
                        
                        let user:User = User(userID: key, username: username, photoURL: photoURL)
                        self.followers.append(user)
                    }
                    self.collectionView.reloadData()
                    }, withCancel: {(error) in
                        print("Error getting friends data: \(error)")
                })
            }
        }
    }
    
    
    func getTheDatas(followings:[TagUser]){
        
        var temporaryCache:NSCache =  NSCache<NSString,AnyObject>()
        
        let dispatchGroup = DispatchGroup()
        //   self.following.removeAll()
        //  self.following.reserveCapacity(followings.count)
        print("Self.following.count: \(self.following.count)")
        for following in followings{
            print("followings: -> \(following)")
        }
        
        print("Current count in self.following: \(self.following.count)")
        // self.collectionView.reloadData()
        print("Number of items in section 0: \(self.collectionView.numberOfItems(inSection: 0))")
        for i in 0..<followings.count{
            
            dispatchGroup.enter()
            // if let user = User.followingCache.object(forKey: follow.ID as NSString){
            if let user = User.signedInUserFollowingCache.object(forKey: followings[i].ID as NSString) as? User{
                //look into removing append
                //self.following[i] = user
                temporaryCache.setObject(user, forKey: user.userID as NSString)
                print("Failed here in user cache")
                //self.collectionView.insertItems(at: [IndexPath(item: self.following.count - 1, section: 0)])
                print("appended user from cache: \(user.userID)")
                dispatchGroup.leave()
            }//else if let tag = Tag.tagsCache.object(forKey: follow.ID as NSString){
            else if let tag = User.signedInUserFollowingCache.object(forKey: followings[i].ID as NSString) as? Tag{
                // self.following[i] = tag
                temporaryCache.setObject(tag, forKey: tag.tagID as NSString)
                print("failed here in tag cached")
                //self.collectionView.insertItems(at: [IndexPath(item: self.following.count - 1, section: 0)])
                print("appended tag from cache: \(tag.tagID)")
                dispatchGroup.leave()
            }
            else{
                //  print("In else: \(follow)")
                print("User not in the cache: \(followings[i])")
                var ref:DatabaseReference!
                if(followings[i].type == .user){
                    ref = Database.database().reference(withPath: "Users")
                    print("searching for user: \(followings[i].ID)")
                }else{
                    ref = Database.database().reference(withPath: "TagIDs")
                    print("searching for tag: \(followings[i].ID)")
                }
                print("searching for follow.id: \(followings[i].ID)")
                print("Entered following dispatchgroup")
                //  dispatchGroup.enter()
                ref.queryOrderedByKey().queryEqual(toValue: followings[i].ID).observeSingleEvent(of: .value, with: {[weak self] (snapshot) in
                    guard let self = self else{
                        assert(true,"self found nil")
                        print("self found nil")
                        return
                    }
                    //  print("Friends data: \(snapshot.value)")
                    let data = snapshot.value as!  [String:Any]
                    print("folloeID: \(followings[i].ID)  -> Snapshot returned: \(data)")
                    
                    for(key,value) in data{
                        //print("Key: \(key) => value: \(value)")
                        let element = value as! [String:Any]
                        
                        if followings[i].type == .user{
                            guard let username = element["username"] as? String, let photoURL = element["photoURL"] as? String else{
                                print("Could not parse all of the values")
                                break
                            }
                            
                            let user:User = User(userID: key, username: username, photoURL: photoURL)
                            
                            temporaryCache.setObject(user, forKey: user.userID as NSString)
                            print("added to temporary cache")
                        }
                        else{
                            guard let name = element["name"] as? String, let location = element["location"] as? [String:Any],let storageRef = element["storageRef"] as? String, let photoURL = element["photoURL"] as? String else{
                                print("Error parsing tag values")
                                return
                            }
                            let tag = Tag(name: name, location: location, tagID: key, storageRef: storageRef,photoURL:photoURL)
                            temporaryCache.setObject(tag!, forKey: key as NSString)
                            print("added to temporary cache")
                        }
                        
                    }
                    
                    print("left dispatch group")
                    dispatchGroup.leave()
                    
                    }, withCancel: {(error) in
                        print("Error getting friends data: \(error)")
                })
            }
            //self.collectionView.reloadData()
        }
        
        
        dispatchGroup.notify(queue: .main, execute: {[weak self] ()in
            guard let self = self else{
                return
            }
            self.following.removeAll()
            for following in followings{
                if let user = temporaryCache.object(forKey: following.ID as NSString) as? User{
                    self.following.append(user)
                    if self.userID == User.signedInUserID{
                        print("User added to signedInUserFollowingCache: \(user.userID)")
                        User.signedInUserFollowingCache.setObject(user, forKey: user.userID as NSString)
                    }
                }else if let tag = temporaryCache.object(forKey: following.ID as NSString) as? Tag{
                    self.following.append(tag)
                    if self.userID == User.signedInUserID{
                        print("Tag added to signedInUserFollowingCache: \(tag.tagID)")
                        User.signedInUserFollowingCache.setObject(tag, forKey: tag.tagID as NSString)
                    }
                }else{
                    print("object: \(following.ID) wasn't found cache")
                }
            }
                        self.collectionView.reloadData()
//            self?.collectionView.performBatchUpdates({
//                [weak self] in
//                // self.collectionView.reloadData()
//
//                guard let self = self else{
//                    assert(true,"self found nil")
//                    print("self found nil")
//                    return
//                }
//                print("in notify dispatch group: \(followings.count)")
//
//                var removeIndicies:[IndexPath] = [IndexPath]()
//                for i in 0..<self.following.count{
//                    removeIndicies.append(IndexPath(item: i, section: 0))
//                }
//
//                self.following.removeAll()
//
//                for following in followings{
//                    if let user = temporaryCache.object(forKey: following.ID as NSString) as? User{
//                        self.following.append(user)
//                        if self.userID == User.signedInUserID{
//                            print("User added to signedInUserFollowingCache: \(user.userID)")
//                            User.signedInUserFollowingCache.setObject(user, forKey: user.userID as NSString)
//                        }
//                    }else if let tag = temporaryCache.object(forKey: following.ID as NSString) as? Tag{
//                        self.following.append(tag)
//                        if self.userID == User.signedInUserID{
//                            print("Tag added to signedInUserFollowingCache: \(tag.tagID)")
//                            User.signedInUserFollowingCache.setObject(tag, forKey: tag.tagID as NSString)
//                        }
//                    }else{
//                        print("object: \(following.ID) wasn't found cache")
//                    }
//                }
//
//                print("current segment selected: \(self.segmentSelected)")
//
//                var indicies:[IndexPath] = [IndexPath]()
//                for i in 0..<self.following.count{
//                    let index = IndexPath(item: i, section: 0)
//                    indicies.append(index)
//                }
//
//                for cell in followings{
//                    print("cell in followings: \(cell.type)")
//                }
//
//                for cell in self.following{
//                    if let cell = cell as? User{
//                        print("cell in self.following: \(cell.userID)")
//                    }else if let cell = cell as? Tag{
//                        print("cell in self.following: \(cell.tagID)")
//                    }
//                }
//
//                print("Indicies to be removed: \(removeIndicies)")
//
//                self.collectionView.deleteItems(at: removeIndicies)
//                self.collectionView.insertItems(at: indicies)
//                temporaryCache.removeAllObjects()
//            }, completion: { (completed) in
//                print("completed batch updates")
//            })
            
            
            
            //change to perform batch updates after
        })
        // collectionView.reloadData()
    }
    
    
}
