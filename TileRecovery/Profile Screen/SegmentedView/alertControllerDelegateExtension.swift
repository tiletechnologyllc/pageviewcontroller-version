//
//  alertControllerDelegateExtension.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit


protocol alertControllerPhotoAlbumDelegate: class {
    func showPhotoAlbumAlertControllerDelegate(sender:segmentedViewController)
}

protocol alertControllerCameraDelegate: class{
    func showCameraAlertControllerDelegate(sender:segmentedViewController)
}


enum photoAlbumSender{
    case feedPostImage
    case profileImage
}

enum cameraSender{
    case feedScreenCamera
    case profileImage
}

