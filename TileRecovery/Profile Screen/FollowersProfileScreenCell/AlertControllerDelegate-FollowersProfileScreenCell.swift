//
//  AlertControllerDelegate-FollowersProfileScreenCell.swift
//  TileRecovery
//
//  Created by 123456 on 3/25/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol AlertControllerDelegate: class{
    func presentAlertController(alertController:UIAlertController, completion:(()->Void)?)
}

extension MainViewController:AlertControllerDelegate{
    func presentAlertController(alertController: UIAlertController,completion:(() -> Void)? = nil) {
        self.present(alertController, animated: true, completion: {
            print("Alert View Controller Presented")
            if let completion = completion{
                completion()
            }
        })
    }
}
