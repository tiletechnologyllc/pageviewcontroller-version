//
//  MenuTableViewDelegate.swift
//  TileRecovery
//
//  Created by 123456 on 3/25/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol  MenuTableViewDelegate: class{
    func addMenuTableView(menuTableViewController:UIViewController)->UIView
    func removeMenuTableView(menuTableViewController:UIViewController)
}

extension MainViewController:MenuTableViewDelegate{
    
    func removeMenuTableView(menuTableViewController:UIViewController) {
        menuTableViewController.willMove(toParent: nil)
        menuTableViewController.view.removeFromSuperview()
        menuTableViewController.removeFromParent()
    }
    
    func addMenuTableView(menuTableViewController: UIViewController) -> UIView {
        self.addChild(menuTableViewController)
             menuTableViewController.didMove(toParent: self)
        guard let menuTableView = menuTableViewController.view else{
            print("Menu tableView empty")
            return UIView()
        }
   
        return menuTableView
    }
    
}
