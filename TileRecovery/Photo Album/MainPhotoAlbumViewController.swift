
import UIKit

class MainPhotoAlbumViewController: UIViewController {
    
    
    
    var slidingView = { () -> UIView in
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .blue
        return view
    }()
    
    var tapToSlide:UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .orange
        btn.setTitle("Tap To Slide", for: .normal)
        //btn.setImage(#imageLiteral(resourceName: "Screen Shot 2018-01-25 at 5.24.37 PM"), for: .normal)
        return btn
    }()
    
    var slidingViewBottomConstraint:NSLayoutConstraint!
    var titleBarView:UIView!
    
    var slideUP = true
    var firstSlide = true
    var centerSliding:CGPoint!
    var counter:Int = 0
    var slidingPanGesture: UIPanGestureRecognizer!
    var mainViewPanGesture:UIPanGestureRecognizer!
    var originalCenter:CGPoint!
    let VC = PhotoAlbumViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Im called too")
        addChild(VC)
        VC.view.translatesAutoresizingMaskIntoConstraints = false
        self.slidingView.addSubview(VC.view)
        self.slidingView.didAddSubview(VC.view)
        VC.didMove(toParent: self)
        self.view.backgroundColor = UIColor.gray
        
        self.view.addSubview(self.slidingView)
        self.view.addSubview(self.tapToSlide)
        
        slidingPanGesture =  UIPanGestureRecognizer(target: self , action: #selector(slidingViewPanGesture(_:)))
        mainViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(mainViewPanGesture(_:)))
        tapToSlide.addTarget(self, action: #selector(tapToSlideTapped(_:)), for: .touchUpInside)
        self.slidingView.addGestureRecognizer(slidingPanGesture)
        self.view.addGestureRecognizer(mainViewPanGesture)
        setUpLayout()
        
        slidingView.bringSubviewToFront(VC.view)
        
        titleBarView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: (7/8)*UIScreen.main.bounds.maxY))
        self.view.addSubview(titleBarView)
        
    }
    
    
    func setUpLayout(){
        //slidingView
        slidingViewBottomConstraint = slidingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: (7/8)*UIScreen.main.bounds.height)
        slidingViewBottomConstraint.isActive = true
        slidingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        slidingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        slidingView.heightAnchor.constraint(equalToConstant: (7/8)*UIScreen.main.bounds.maxY).isActive = true
        //  slidingView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.maxY).isActive = true
        
        //button
        tapToSlide.leadingAnchor.constraint(equalTo: self.view.layoutMarginsGuide.leadingAnchor, constant: 0).isActive = true
        tapToSlide.bottomAnchor.constraint(equalTo: self.view.layoutMarginsGuide.bottomAnchor, constant: -20).isActive = true
        tapToSlide.widthAnchor.constraint(equalToConstant: 120).isActive = true
        tapToSlide.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //collectionView for the photo album
        self.VC.view.leadingAnchor.constraint(equalTo: slidingView.leadingAnchor, constant: 0).isActive = true
        self.VC.view.trailingAnchor.constraint(equalTo: slidingView.trailingAnchor, constant: 0).isActive = true
        self.VC.view.bottomAnchor.constraint(equalTo: slidingView.bottomAnchor, constant: 0).isActive = true
        self.VC.view.topAnchor.constraint(equalTo: slidingView.topAnchor, constant: 0).isActive = true
    }
    
    @objc func slidingViewPanGesture(_ sender:UIPanGestureRecognizer){
        let translation = sender.translation(in: self.view)
        
        if sender.state == UIGestureRecognizer.State.began{
            originalCenter = self.slidingView.center
        }
        
        if translation.y >= 0{
            self.slidingView.center = CGPoint(x: self.slidingView.center.x, y: self.slidingView.center.y + translation.y)
            sender.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if slidingPanGesture.state == UIGestureRecognizer.State.ended{
            if translation.y == 0{
                UIView.animate(withDuration: 1.0, animations: {
                    self.slidingView.transform = CGAffineTransform(translationX: 0, y: self.slidingView.frame.height)
                }, completion: nil)
                self.slideUP = !self.slideUP
            }
            else{
                UIView.animate(withDuration: 0.5, animations: {
                    self.slidingView.center = self.centerSliding
                }, completion: nil)
            }
        }
    }
    
    @objc func tapToSlideTapped(_ sender:UIButton){
        if firstSlide{
            centerSliding = self.slidingView.center
            firstSlide = false
        }
        else{
            self.slidingView.center = centerSliding
        }
        if(slideUP){
            UIView.animate(withDuration: 1.0, animations: {
                self.slidingView.transform = CGAffineTransform(translationX: 0, y: -(7/8)*UIScreen.main.bounds.height)
            }, completion: nil)
        }
        else{
            UIView.animate(withDuration: 1.0, animations: {
                self.slidingView.transform = CGAffineTransform.identity
            }, completion: nil)
        }
        slideUP = !slideUP
    }
    
    @objc func mainViewPanGesture(_ sender:UIPanGestureRecognizer){
        if sender.state == UIGestureRecognizer.State.ended{
            if firstSlide{
                centerSliding = self.slidingView.center
                firstSlide = false
            }
            else{
                self.slidingView.center = centerSliding
            }
            UIView.animate(withDuration: 1.0, animations: {
                self.slidingView.transform = CGAffineTransform(translationX: 0, y: -(7/8)*UIScreen.main.bounds.height)
            }, completion: nil)
            slideUP = !slideUP
        }
    }
    
    
    
}
