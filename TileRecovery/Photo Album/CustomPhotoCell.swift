//
//  CustomPhotoCell.swift
//  TileRecovery
//
//  Created by 123456 on 3/4/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit
class customPhotoCell:UICollectionViewCell{
    
    var representedAssetIdentifier: String!
    
    var imageView:UIImageView = UIImageView()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        print("Inside of init")
        
        imageView.backgroundColor = .orange
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        self.addSubview(imageView)
        
        self.layer.cornerRadius = 10.0
        imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
