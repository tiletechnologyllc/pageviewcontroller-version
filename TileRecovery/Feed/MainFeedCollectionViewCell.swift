import UIKit
import FirebaseDatabase
import FirebaseStorage
import FirebaseUI
import FirebaseFunctions

protocol  CollectionCellDelegate: class {
    func selectedItem(index:IndexPath,senderCellType:CellTypes,name:String,id:String)
}

protocol superViewDelegate: class {
    func getSuperViewLayout()->UILayoutGuide
}

class MainFeedCollectionViewCell: UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDataSourcePrefetching{
    
    
    
    weak var delegate: CollectionCellDelegate?
    
    weak var viewDelegate:superViewDelegate?{
        didSet{
            print("The delegate of the MainFeedColelctionViewcell was set")
        }
    }
    

    weak var reloadSegmentedViewControllerDelegate:ReloadSegmentedViewControllerDelegate?
    
    var FeedCollectionView: UICollectionView!
    
//    var selectedTag:Tag?{
//        didSet{
//            print("TagID has been set im FeedCollectionView:\(selectedTag?.tagID)")
//            if let tag = selectedTag{
//                self.getPosts(tagID: tag.tagID)
//            }else{
//                print("unable to read tagID: \(selectedTag?.tagID)")
//            }
//        }
//    }
    
    var tagID:String?{
                didSet{
                    print("TagID has been set im FeedCollectionView:\(tagID)")
                    if let tagID = tagID{
                        self.getPosts(tagID: tagID)
                    }else{
                        print("unable to read tagID: \(tagID)")
                    }
                }
    }
    
    
    var tagName:String?{
        didSet{
            self.FeedCollectionView.reloadData()
        }
    }
    var fetchingMore = false
    var endReached = false
    
    var posts = [Post]()
    var storage:Storage!
    var database:Database!
    var lastQueryKey:String? = nil{
        didSet{
            print("last query key: \(lastQueryKey)")
            
            guard var unformattedKey = lastQueryKey else{
                return
            }
            let lastCharacter = String(unformattedKey.removeLast())
            print("unformattedKey: \(unformattedKey)")
            var ascii = UnicodeScalar(lastCharacter)!.value
            ascii -= 1
//            switch ascii {
//            //ascii code for .
//            case 46:
//                ascii = 48
//            case 58:
//                ascii = 65
//            case 90:
//                ascii = 97
//            case 122,127:
//                ascii = 126
//            default:
//                break
//            }
            let lastChar:String = String(UnicodeScalar(ascii)!)
            let formattedKey = unformattedKey + lastChar
            print("formatted key: \(formattedKey)")
            lastQueryKey = formattedKey
        }
    }
    func getPosts(tagID:String,lastPost:Post? = nil, numberToGrab:Int? = nil){
        print("TagID in get Posts: \(tagID)")
        fetchingMore = true
        // self.database.reference(withPath: "TagIDs/\(tagID)/posts").queryOrdered(byChild: "weightedLikes").observeSingleEvent(of: .value, with: {(datasnapshot) in
        
        //      self.database.reference(withPath: "PostCollections/\(tagID)").queryOrdered(byChild: "weightedLikes").observeSingleEvent(of: .value, with: {(datasnapshot) in
//        if var key:String = self.posts[self.posts.count - 1].postID as? String{
//            print("last search key: \(key)")
//            let lastCharacter = String(key.removeLast())
//            var ascii = UnicodeScalar(lastCharacter)!.value
//            ascii += 1
//            let lastChar:String = String(UnicodeScalar(ascii)!)
//            key = key + lastChar
//            formattedKey = key
//        }

        var limitAmount:UInt = 2
        if let numberToGrab = numberToGrab as? UInt{
            limitAmount = UInt(numberToGrab)
        }
        
        var query:DatabaseQuery!
        print("lastQueryKey before query: \(lastQueryKey)")
        if var lastPost = lastPost as? Post,var unformattedKey = lastQueryKey/*lastPost.postID*/,let weightedLikes = lastPost.weightedLikes as? Int{
             print("original unformattedKey: \(unformattedKey)")
//            let lastCharacter = String(unformattedKey.removeLast())
//            print("unformattedKey: \(unformattedKey)")
//            var ascii = UnicodeScalar(lastCharacter)!.value
//            ascii += 1
//            let lastChar:String = String(UnicodeScalar(ascii)!)
//            let formattedKey = unformattedKey + lastChar
           // print("formatted key: \(formattedKey)")
            let formattedKey = unformattedKey
            print("formatted key before query: \(formattedKey)")
            query = self.database.reference(withPath: "TagPosts/\(tagID)").queryOrdered(byChild: "weightedLikes").queryEnding(atValue: weightedLikes, childKey: formattedKey).queryLimited(toLast: limitAmount)//.queryLimited(toLast: 2)
            print("Query about to be preformed: \(query)")
        }else{
              query = self.database.reference(withPath: "TagPosts/\(tagID)").queryOrdered(byChild: "weightedLikes").queryLimited(toLast: 8)
            print("First round through")
        }
        
        var tempArr:[Post] = [Post]()
        query.observeSingleEvent(of: .value, with: {(datasnapshot) in
            print("snapshot ref: \(datasnapshot.ref), snapshot.count: \(datasnapshot.childrenCount)")
            if datasnapshot.exists(){
                for child in datasnapshot.children{
                    if let child = child as? DataSnapshot, let data = child.value as? [String:Any]{
                        var isUser:Bool = false
                        if let isuser = data["isUser"] as? Bool{tempArr.removeAll()
                            isUser = isuser
                        }
                        print("Data: \(data)")
                        guard let likes = data["totalLikes"] as? Int, let storageRef = data["storageRef"] as? String, let userID = data["userID"] as? String,let photoURL = data["photoURL"] as? String, let weightedLikes = data["weightedLikes"] as? Int else{
                            print("Error getting value from data")
                            continue
                        }
                        print("child key: \(child.key)")
                        let post = Post(storageRef: storageRef, totalLikes: likes,postID:child.key,userID:userID,tagID:tagID,isUser:isUser,photoURL:photoURL,weightedLikes:weightedLikes)
                        //self.posts.append(post)
                        tempArr.insert(post, at: 0)
                    }else{
                        print("Failed to parse data to type [String:Any]")
                    }
                }
                self.getUserNames(posts: tempArr,completion: {(usernamePosts) in
                    
                    self.FeedCollectionView.performBatchUpdates({
                        print("tempArr.count: \(tempArr.count)")
                        var indexPaths:[IndexPath] = [IndexPath]()
                        for i in 0..<tempArr.count{
                            let item = i + self.posts.count
                            indexPaths.append(IndexPath(item: item, section: 0))
                        }
                        self.posts.append(contentsOf: tempArr)
                        print("self.posts.count: \(self.posts.count)")
                        self.FeedCollectionView.insertItems(at: indexPaths)
                        self.fetchingMore = false
                        
                        print("lastQueryKey before: \(tempArr[tempArr.count - 1].postID)")
                        self.lastQueryKey = tempArr[tempArr.count - 1].postID
                        print("lastQueryKey after: \(self.lastQueryKey)")
                        //tempArr.removeAll()
                    }, completion: {(completed) in
                        print("batch updates completed: \(completed)")
                    })
                    
                })
                
               
                print("self.posts: \(self.posts)")
//                if var key:String = self.posts[self.posts.count - 1].postID as? String{
//                    print("last search key: \(key)")
//                    let lastCharacter = String(key.removeLast())
//                    var ascii = UnicodeScalar(lastCharacter)!.value
//                    ascii += 1
//                    let lastChar:String = String(UnicodeScalar(ascii)!)
//                    key = key + lastChar
//                    self.formattedKey = key
//                }
                
                
                
                }
            else{
                self.endReached = true
                print("Data snap shot doesnt exist no posts available")
            }
//            for post in self.posts{
//                print("postUsername: \(post.userName)")
//            }
          //  self.FeedCollectionView.reloadData()
        }, withCancel: {(error) in
            print("query: \(query.ref)")
            print("An error has occured while pulling from firebase: \(error.localizedDescription)")
        })
    }
    
    func getUserNames(posts postsBefore: [Post],completion: (([Post])->Void)?){
        print("inside of get usernames posts.count: \(posts.count)")
        let dispatchGroup:DispatchGroup = DispatchGroup()
        for post in postsBefore{
            dispatchGroup.enter()
            print("postID: \(post.postID), userID: \(post.userID)")
            Database.database().reference(withPath: "Users/\(post.userID!)").observe(.value, with: {(snapshot) in
                if snapshot.exists(){
                    
                    guard let data = snapshot.value as? [String:Any],let username = data["username"] as? String else{
                        print("failed to parse username data")
                        return
                    }
                   
                    print("username: \(username)")
                    post.userName = username
                    print("Post about to be set to the cache: \(post.postID), tagID: \(post.tagID)")
                    Post.setPostToCache(post: post, tagUserID: post.tagID!)
                }
                
                else{
                    print("Snapshot path: \(snapshot.ref),snapshot.exists() \(snapshot.exists())")
                    print("snapshot doesnt exist")
                }
               // self.posts = posts
//                for post in self.posts{
//                    print("username: \(post.userName)")
//                }
//                self.fetchingMore = false
//                self.FeedCollectionView.reloadData()
                print("about to call completion")
  
                dispatchGroup.leave()
            }, withCancel: {(error) in
                print("Error getting usernames: \(error)")
                dispatchGroup.leave()
            })
        }
        dispatchGroup.notify(queue: .main, execute: {
            if let completion = completion{
                completion(postsBefore)
            }
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var doesSignedInUserFollow:Bool!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.posts.removeAll()
        //    }
        
        self.database = Database.database()
        self.storage = Storage.storage()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        // layout.sectionInset = UIEdgeInsets(top: 116, left: 0, bottom: 10, right: 0)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width-4)/2, height: (UIScreen.main.bounds.width-4)/2)
        layout.minimumInteritemSpacing = 4
        layout.minimumLineSpacing = 4
        self.FeedCollectionView = UICollectionView(frame: CGRect(), collectionViewLayout: layout)
        self.addSubview(FeedCollectionView
        )
        //    override func awakeFromNib() {
        //        super.awakeFromNib()
        FeedCollectionView.translatesAutoresizingMaskIntoConstraints = false
        FeedCollectionView.delegate = self
        FeedCollectionView.dataSource = self
        FeedCollectionView.prefetchDataSource = self
        
        
        // Do any additional setup after loading the view.
        //self.collectionView?.reloadData()
        //Undo after Posts is added
        //self.FeedCollectionView?.backgroundColor = colors.backgroundColor
        self.FeedCollectionView.allowsSelection = true
        self.FeedCollectionView.backgroundColor = colors.backgroundColor
        self.FeedCollectionView.register(InsideFeedCollectionViewCell.self, forCellWithReuseIdentifier: "InsideFeedCollectionViewCell")
        
        self.FeedCollectionView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.FeedCollectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.FeedCollectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.FeedCollectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        
        print("In awakeFromNib \(imagePost.defaultImages.count)")
        
        //Added in header
        FeedCollectionView.register(MainFeedScreenHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "MainFeedScreenHeader")
        
        print("TagID in feedscreen: \(self.tagID)")//self.selectedTag?.tagID)")
//        if let tag = self.selectedTag{
//            //  getPosts(tagID: tagID)
//        }else{
//            print("TagID is nil")
//        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width-4)/2, height: (UIScreen.main.bounds.width-4)/2)
    }
    
//    @objc func followButtonTapped(){
//        print("Follow button tapped")
//    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("number of items: \(posts.count)")
        return posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = FeedCollectionView.dequeueReusableCell(withReuseIdentifier: "InsideFeedCollectionViewCell", for: indexPath) as! InsideFeedCollectionViewCell
        //print("Posts for \(self.selectedTag?.name) => tagID: \(self.selectedTag?.tagID)")
//        for post in posts{
//            print("post: \(post.storageRef)")
//        }
        //
        //cell.cellImageView.clipsToBounds = true
        //        //cell.feedCellImageView.layer.cornerRadius = CGFloat((cell.feedCellImageView.frame.width)/5)
        //        cell.cellImageView.image = imagePost.defaultImages[indexPath.item].image
        //        print("feed cell actual width: \(cell.frame.width)")
        //        print("feed cell actual height: \(cell.frame.height)")
        //        print("feed cell corner radius: \(cell.cellImageView.layer.cornerRadius )")
        
        //
        //        cell.cellImageView.contentMode = .scaleAspectFill
        //
        //        guard let image = cell.cellImageView.image else{
        //            print("Error in cellforitemat in SearchfeedScreena cella")
        //            return UICollectionViewCell()
        //        }
        //
        //        let photoAspectRatio:CGFloat = image.size.width/image.size.height
        //        let cellAspectRatio:CGFloat = cell.bounds.width/cell.bounds.height
        //        if photoAspectRatio >= cellAspectRatio{
        //            cell.cellImageView.frame = CGRect(x: 0, y: 0, width: photoAspectRatio*cell.bounds.height, height: cell.bounds.height)
        //        }
        //        else{
        //            cell.cellImageView.frame = CGRect(x: 0, y: 0, width: cell.bounds.width, height: cell.bounds.width/photoAspectRatio)
        //        }
        //
        
        let placeholder:UIImage = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
        
        let storagePath = posts[indexPath.item].storageRef
        let storageReference = self.storage.reference(withPath: storagePath)
        cell.cellImageView.sd_setImage(with: storageReference, placeholderImage: placeholder, completion: {(image,error,imageCacheType,storageReference) in
            if let error = error{
                print("Uh-Oh an error has occured: \(error.localizedDescription)" )
            }
            self.setImageToAspectRatio(image: image, imageView: cell.cellImageView, bounds: cell.bounds)
            self.posts[indexPath.item].image = image
        })
      
        
        //cell.cellImageView.layer.cornerRadius = CGFloat((cell.cellImageView.frame.width)/5)
        cell.cellImageView.layer.cornerRadius  = 29.6
        cell.cellImageView.clipsToBounds = true
        cell.layer.cornerRadius = 29.6
        return cell
    }
    
    func setImageToAspectRatio(image:UIImage?,imageView:UIImageView,bounds:CGRect){
        guard let image = image else{
            print("Image was nil")
            let placeHolder = #imageLiteral(resourceName: "Screen Shot 2018-01-29 at 8.30.24 PM")
            imageView.image = placeHolder
            return
        }
        let photoAspectRatio = image.size.width/image.size.height
        let cellAspectRatio = bounds.width/bounds.height
        if photoAspectRatio >= cellAspectRatio{
            imageView.frame = CGRect(x: 0, y: 0, width: photoAspectRatio*bounds.height, height: bounds.height)
        }
        else{
            imageView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.width/photoAspectRatio)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        var urls:[URL] = [URL]()
        for indexPath in indexPaths{
            if let url = self.posts[indexPath.item].url{
                urls.append(url)
            }
        }
////        let lastIndexPathItem = indexPaths.last?.item
//        print("Index items in prefetch")
//        guard let tagID = self.tagID, let lastPost = self.posts.last else{
//            print("TagID was empty")
//            return
//        }
//
//        if fetchingMore == false{
//            self.getPosts(tagID: tagID, lastPost: lastPost, numberToGrab: 2)
//        }
        
       SDWebImagePrefetcher.shared().prefetchURLs(urls)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
       
            if offsetY > contentHeight - scrollView.frame.height * 4 {
                if fetchingMore == false && endReached == false{
                     print("did scroll called")
                    let lastPost = self.posts[posts.count - 1]
                    guard let tagID = self.tagID else{
                        return
                    }
                    print("before call last query key: \(self.lastQueryKey)")
                    getPosts(tagID: tagID, lastPost: lastPost, numberToGrab: 2)
                }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        
        
        print("Cell selected in\n\t section: \(indexPath.section)\n\titem: \(indexPath.item)")
        self.delegate?.selectedItem(index:indexPath,senderCellType:CellTypes.MainFeedCollectionViewCell,name:(tagName)!,id:(tagID)!)
        
        if let tagID = self.tagID{
            Post.cache.setObject(self.posts as NSArray, forKey: tagID as NSString)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "MainFeedScreenHeader", for: indexPath) as! MainFeedScreenHeader
        if let selectedTag = self.tagName{
            header.headerLabel.text = selectedTag//"Tile Tag Goes Here"
        }else{
            header.headerLabel.text = "Tile Tag Goes Here"
            print("No name for tile tag")
        }
        header.backgroundColor = colors.backgroundColor
        
        let viewLayout:UILayoutGuide = (viewDelegate?.getSuperViewLayout())!
        
        header.headerLabel.translatesAutoresizingMaskIntoConstraints = false
        header.headerLabel.topAnchor.constraint(equalTo: (viewLayout.topAnchor), constant: 10).isActive = true
        header.headerLabel.leadingAnchor.constraint(equalTo: (header.leadingAnchor), constant: 16).isActive = true
        header.headerLabel.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        guard let tagID = self.tagID else{
            print("TagID was found nil in header")
            return header
        }
        
        header.reloadSegmentedViewControllerDelegate = self.reloadSegmentedViewControllerDelegate
        header.tagID = tagID
        header.followButtonTappedDelegate = self
        header.setUpFollowButton()
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 116)
    }
}

extension MainViewController:superViewDelegate{
    func getSuperViewLayout()-> UILayoutGuide {
        print("In get superViewlayout")
        return self.view.safeAreaLayoutGuide
    }
}
