//
//  FollowButtonTappedDelegate.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 2/10/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol FollowButtonTappedDelegate: class{
    func followButtonTapped(doesUserFollow:Bool)
}

extension MainFeedCollectionViewCell:FollowButtonTappedDelegate{
    func followButtonTapped(doesUserFollow:Bool) {
        print("Follow Button Tapped inside of delegate")
        self.doesSignedInUserFollow = doesUserFollow
        print("does signed in user follow: \(self.doesSignedInUserFollow)")
        //Questionable reload segmentedViewController
    }
}
