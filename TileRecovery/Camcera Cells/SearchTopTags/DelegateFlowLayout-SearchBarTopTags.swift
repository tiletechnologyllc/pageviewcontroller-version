//
//  DelegateFlowLayout-SearchBarTopTags.swift
//  TileRecovery
//
//  Created by Thomas M. Jumper on 9/20/18.
//  Copyright © 2018 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

extension SearchBarTopTagsViewController:UICollectionViewDelegateFlowLayout{
    
    func setUpLayout(){
        //Top Tags CollectionView
        topTagsCollectionView.contentInset = UIEdgeInsets(top: 2, left: 2, bottom: -2, right: 2)
        topTagsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        topTagsCollectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        topTagsCollectionView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        topTagsCollectionView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        topTagsCollectionView.heightAnchor.constraint(equalToConstant:(self.view.bounds.width/3)+4).isActive = true
        
        //myView
        self.myView.translatesAutoresizingMaskIntoConstraints = false
        self.myView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        self.myView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        self.myView.heightAnchor.constraint(equalToConstant: 98).isActive = true
        self.myView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        
        //PictureView
        self.PictureView.translatesAutoresizingMaskIntoConstraints = false
        self.PictureView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        self.PictureView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        self.PictureView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        self.PictureView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        self.PictureView.backgroundColor = .purple
        self.view.sendSubviewToBack(PictureView)
        
        //Search Tags CollectionView
        self.searchTagsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        self.searchTagsCollectionView.leadingAnchor.constraint(equalTo: self.myView.leadingAnchor, constant: 0).isActive = true
        self.searchTagsCollectionView.trailingAnchor.constraint(equalTo: self.myView.trailingAnchor, constant: 0).isActive = true
        self.searchTagsCollectionView.bottomAnchor.constraint(equalTo: self.myView.bottomAnchor, constant: -4).isActive = true
        self.searchTagsCollectionView.topAnchor.constraint(equalTo: self.searchBar.bottomAnchor, constant: 4).isActive = true
        //self.searchTagsCollectionView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        //Search Bar
        self.searchBar.translatesAutoresizingMaskIntoConstraints = false
        self.searchBar.topAnchor.constraint(equalTo: self.myView.topAnchor, constant: 0).isActive = true
        self.searchBar.leadingAnchor.constraint(equalTo: self.myView.leadingAnchor, constant: 4).isActive = true
        self.searchBar.trailingAnchor.constraint(equalTo: self.myView.trailingAnchor, constant: -4).isActive = true
        self.searchBar.heightAnchor.constraint(equalToConstant: 45).isActive = true
        searchBar.backgroundColor = .white
        searchBar.layer.borderWidth = 1
        searchBar.layer.cornerRadius = searchBar.frame.width/50
        searchBar.layer.borderColor = UIColor.black.cgColor
    }
}

