//
//  SelectedTagDelegate.swift
//  TileRecovery
//
//  Created by 123456 on 2/18/19.
//  Copyright © 2019 Tile Technology LLC. All rights reserved.
//

import Foundation
import UIKit

protocol SelectedSearchTagDelegate: class{
    func selectedSearchTagDelegate(tagName:String)
}

extension PreviewController:SelectedSearchTagDelegate{
    func selectedSearchTagDelegate(tagName: String) {
        print("Selected Search Tag delegate called: \(tagName)")
    }
}
